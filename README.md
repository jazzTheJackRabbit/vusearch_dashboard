Ensure you have Python2.7 and virtualenv setup

To install the server:

```
virtualenv env
env/bin/pip install -r require.txt

npm install
bower install
mv bower_components src/static/
```

Start the server:

```
env/bin/python server/app.py
```
