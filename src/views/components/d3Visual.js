function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
var tsnejs = require('./tsne.js')

var components = {}

var _defaults = {
	'feature_vector_url': '/getCategoryFeatureVectors',
	'categories': [],
	'categories_checked': [1],
	'product_id_to_search': "",
	'tsne_parameters':{
		'epsilon': 10,
		'perplexity': 30,
		'dim': 2,
		'max_iterations': 1000,
	}
}

var MenuBar = React.createClass({
	render: function(){
		return(
			<div className="ui top inverted fixed borderless menu topmenu">
			  <a className="item" href="/">
			    	<img src = "static/images/logo.png" className="ui avatar image" />
			  </a>
			</div>
		)
	}
})

var CategoryFields = React.createClass({
	getInitialState: function(){
		return({
			checked: false
		})
	},
	componentDidMount: function(){
		if(components['visual']){
			if (components['visual'].state['categories_checked'].indexOf(this.props.index)!= -1){
				this.setState({
					checked: true
				})
			}
		}
		else{
			if (_defaults['categories_checked'].indexOf(this.props.index)!= -1){
				this.setState({
					checked: true
				})
			}
		}
	},
	onChange: function(){
		this.setState({
			checked: !this.state.checked
		})
	},
	render: function(){		
		return(
			<div className="field">
				<div className="ui checkbox">
					<input type="checkbox" name="category" value={this.props.index} checked = {this.state.checked} onChange={this.onChange}/>
				  <label>{this.props.name}</label>
				</div>
			</div>
		)
	}
})

var Message = React.createClass({
	render: function(){
		return(
		<div className="ui mini message no-padding">
		  This is a mini message.
		</div>
		)
	}
})

var TSNEFields = React.createClass({
	render: function(){
		return(
			<div className="field">
				<div className="ui fluid labeled input">
				  <div className="ui label">
				    {this.props.name}
				  </div>
				  <input type="text" placeholder={this.props.value} />
				</div>
			</div>
		)
	}
})

var Loader = React.createClass({
	getInitialState: function(){
		return({
			"hidden": false
		})
	},
	render: function(){
		return(
			<div class={this.state.hidden ? "" : "ui loader"}></div>
		)
	}
})

var VerticalMenu = React.createClass({
	getInitialState: function(){
		return({
			categories: _defaults['categories'],
			categories_checked: _defaults['categories_checked'],
			tsne_parameters: _defaults['tsne_parameters']
		})
	},
	handleSubmit: function(){		
		components.visual.setState({
			count: 1,
			feature_vector_url : "/getCategoryFeatureVectors",
		})
		components['vertical-menu'].updateTSNEParameters();				
		

		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		let categoriesChecked = []
		for (let i=0; i<checkboxes.length; i++){
			if (checkboxes[i].checked){
				categoriesChecked.push(i)
			}			
		}
		components['vertical-menu'].setState({
			categories_checked : categoriesChecked
		})
		components['visual'].setState({
			categories_checked : categoriesChecked
		})

		console.log(categoriesChecked)
		if(categoriesChecked.length){			
			components.visual.resetParameters();
			components.visual.startVisualizationProcessing()	
			components.visual.resetMessageViews()
			width = w = $('.ten.wide.column').width() * 0.9
			d3.select('svg').attr('width',w)		
		}
		else{
			components.visual.showNoCategorySelected();
		}
	},
	resetCategories(){
		// Update Categories
		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		for (let i=0; i<checkboxes.length; i++){
			checkboxes[i].checked = false		
		}
		components['vertical-menu'].setState({
			categories_checked : []
		})
		components['visual'].setState({
			categories_checked : []
		})
	},
	resetCategories(){
		// Update Categories
		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		categoriesChecked = []
		for (let i=0; i<checkboxes.length; i++){
			checkboxes[i].checked = false		
		}
	},
	updateCategories(){
		// Update Categories
		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		categoriesChecked = []
		for (let i=0; i<checkboxes.length; i++){
			if (checkboxes[i].checked){
				categoriesChecked.push(i)
			}			
		}
		this.setState({
			categories_checked : categoriesChecked
		})
		components['visual'].setState({
			categories_checked : categoriesChecked
		})
	},
	updateTSNEParameters(){
		// Update TSNE Parameters
		tsne_parameter_fields = this.refs['tsne_parameter_fields']
		tsne_parameters_update ={}
		for (let i=0; i<tsne_parameter_fields.childNodes.length; i++){
			parameter_name = d3.select(tsne_parameter_fields.childNodes[i]).select('div.ui.label').property('innerText')
			parameter_value = d3.select(tsne_parameter_fields.childNodes[i]).select('input').property('value') ? d3.select(tsne_parameter_fields.childNodes[i]).select('input').property('value') :  d3.select(tsne_parameter_fields.childNodes[i]).select('input').property('placeholder')  
			tsne_parameters_update[parameter_name] = parameter_value
		}
		components['visual'].setState({
			tsne_parameters: tsne_parameters_update
		})
	},
	componentDidMount: function(){
		$('.ui.accordion')
		  .accordion('open',0)
		;
		components['vertical-menu'] = this;		
		$.ajax({
			url: "/listCategories",
			method: 'GET',
			success: function(response){
				categories = response.map(function(category){
					category = category.split("-").join(" ")
					return toTitleCase(category)
				})

				components['vertical-menu'].setState({
					categories : categories
				})
			}
		});
	},
	render: function(){
		return(
			<div className="ui big vertical accordion menu">
			  <div className="item">
			    <a className="title">
			      <i className="block layout icon"></i>
			      Choose Categories
			      <i className="dropdown icon"></i>
			    </a>			    
			    <div className="content vertical-menu">
			      <div className="ui form">
			        <div className="grouped fields" ref="category_fields">			        			        
			          {
			          	this.state.categories.map(function(component,index){
			          		return <CategoryFields name={component} index={index}/>
			          	})
			          }		          
			        </div>
			      </div>
			    </div>
			  </div>	
			  <div className="item">
			    <a className="title">
			    	<i className="setting icon"></i>
			      	TSNE Parameters
			      	<i className="dropdown icon"></i>
			    </a>			    
			    <div className="content vertical-menu">
			      <div className="ui form">
			        <div className="grouped fields" ref="tsne_parameter_fields">			        			        
			          {
			          	Object.keys(this.state.tsne_parameters).map(function(tsne_parameter,index){
			          		return <TSNEFields name={tsne_parameter} value={this.state.tsne_parameters[tsne_parameter]}/>
			          	}.bind(this))
			          }		          
			        </div>
			      </div>
			    </div>
			  </div>		  
			  <div className="fluid ui inverted green button item" onClick={this.handleSubmit} ref="update_button">
			  	Update
			  </div>
			</div>			
		)
	}
})

var animatationDuration = 500
var width = w = $("#myDiv").width()
var height = h = 600
var barSpace = 5
var padding = 50 
var Visual = React.createClass({
	getInitialState: function(){
		return({
			count: 1,
			tsne_parameters:  _defaults['tsne_parameters'],
			feature_vector_url: _defaults['feature_vector_url'],
			categories : _defaults['categories'],
			categories_checked: _defaults['categories_checked'],
			product_id_to_search: _defaults['product_id_to_search']
		})
	},
	componentDidMount: function(){
		this.resetParameters();
		this.startVisualizationProcessing();
	},
	startVisualizationProcessing: function(){
		this.loadingStarted()
		setTimeout(this.process_visualization,1000)		
	},
	loadingComplete: function(){
		d3.select('#loader')
			.attr("class","hidden")
		d3.select('#myDiv')
			.classed("hidden",false).attr("class","ui fluid container padded segment")

		d3.select(components['vertical-menu'].refs['update_button']).attr("class","ui green inverted fluid button item")
	},
	loadingStarted: function(){
		d3.select('#loader')
			.classed("hidden", false).attr("class","ui active dimmer");

		d3.select('#myDiv')
			.attr("class","hidden")

		d3.select(components['vertical-menu'].refs['update_button']).attr("class","ui inverted fluid button item disabled")
	},
	resetParameters: function(){
		d3.selectAll("svg > *").remove();
	},
	showNoCategorySelected: function(){
		d3.select('#empty_category_message')
			.attr('class','ui red message')

		d3.select('#myDiv')
			.classed('hidden','true')
	},
	resetMessageViews: function(){
		d3.select('#empty_category_message')
			.classed('hidden','true')
	},
	process_visualization: function(){		
		function compare(field){
			return function sort_compare(a,b) {
			  if (a[field] < b[field])
			    return -1;
			  if (a[field] > b[field])
			    return 1;
			  return 0;
			}
		}
		
		var self = this;
		components['visual'] = self;
		
		data = JSON.stringify({
			"categories_checked":self.state['categories_checked']
		})

		if(self.state.feature_vector_url == '/getSimilarProducts'){
			data = JSON.stringify({
				"product":self.state['product_id_to_search']
			})
		}
	
		if(self.state['categories_checked'].length || data['product'] != ""){
			$.ajax({
				url: self.state.feature_vector_url,
				method: 'POST',
				data: data,
				contentType: 'application/json',
				success: function(response){
					var categories = response.results;	
					categories = categories.sort(compare("Id"))
					var ids = []
					var featureVectors = []

					for (i in categories){
						ids.push(categories[i]['Id'])
						featureVectors.push(categories[i]['FeatureVector'])
					}

					data = JSON.stringify({
									"Ids":ids
								})
					$.ajax({
						url: "/getProducts",
						dataType: "json",
						data: data,
						method: 'POST',
						success: function(response){
							// Remove the loading screen
							self.loadingComplete()
							var products = response.results;
							products = products.sort(compare("Id"))
							// TODO: cases when ids are not returned?
							// for(var i in products){
							// 	products.productNames.push(productsRaw[i]['Name'])
							// 	products.productPrices.push(productsRaw[i]['Price'])
							// 	products.imageURLs.push(productsRaw[i]['ImageUrl'])
							// }

							// Start TSNE
							var opt = {}
							opt.epsilon = self.state.tsne_parameters.epsilon; // epsilon is learning rate (10 = default)
							opt.perplexity = self.state.tsne_parameters.perplexity; // roughly how many neighbors each point influences (30 = default)
							opt.dim = self.state.tsne_parameters.dim; // dimensionality of the embedding (2 = default)

							var tsne = new tsnejs.tSNE(opt); // create a tSNE instance

							tsne.initDataRaw(featureVectors);

							var count = self.state.count
							var max_iterations = self.state.tsne_parameters.max_iterations

							// drawPlot here
							var xScale = d3.scaleLinear()
							var yScale = d3.scaleLinear()
							var rScale = d3.scaleLinear()

							tsne.step()
							var Y = tsne.getSolution()
							var dataset = []
							for (index in Y){
								var point = Y[index]
								dataset.push([point[0],point[1]])
							}

							self.resetScales(dataset,xScale,yScale,rScale)

							// Drawing code 
							var svg = d3.select('svg')
							if(svg.empty()){
								svg = d3.select('#myDiv')
										.append('svg')
										.attr('width',w)
										.attr('height',h)
										.attr('style','overflow:overlay')
							}						

							// Scatter plot dots/circle layer
							// var circles = svg.selectAll('circle')
							// 	.data(data.users)
							// 	.enter()
							// 		.append('circle')
							var images = svg.selectAll('image')
								.data(products)
								.enter()
									.append('image')
							// var textLabels = svg.selectAll('text')
							// 	.data(data.users)
							// 	.enter()
							// 		.append('text')
							
							self.applyImageAttrs(images,products,Y,xScale,yScale,rScale)
							// self.applyCircleAttrs(circles,Y,xScale,yScale,rScale)
							// self.applyTextAttrs(textLabels,Y,xScale,yScale,rScale)

							self.setState({
								count: count,
								max_iterations: max_iterations
							})

							intervalID = setInterval(function(){
								if(count <= max_iterations){
									var cost = tsne.step()
									var Y = tsne.getSolution()
									self.plot_data(products,Y,xScale,yScale,rScale)
									self.setState({
										count: count++
									})
								}	
								else{
									clearInterval(intervalID)
								}	
							},0)
						}
					})
				}
			});
		}
		else{
			self.loadingComplete()
			self.showNoCategorySelected()
		}
		
		// d3.json("/static/data/categories.json",function(response){
		// }.bind(this))

		// initialize data. Here we have 3 points and some example pairwise dissimilarities
		// d3.json("/static/data/out2.json",function(data){
		 // }.bind(this))
	},
	plot_data: function(products,Y,xScale,yScale,rScale){
		var dataset = []
		for (index in Y){
			var point = Y[index]
			dataset.push([point[0],point[1]])
		}
		var svg = d3.select('svg')
		var circles = svg.selectAll('circle')
			.data(products)
		var images = svg.selectAll('image')
			.data(products)
		// var textLabels = svg.selectAll('text')
		// 	.data(data.users)

		this.resetScales(dataset,xScale,yScale,rScale)
		this.applyImageAttrs(images,products,Y,xScale,yScale,rScale)
		// this.applyCircleAttrs(circles,Y,xScale,yScale,rScale)
		// this.applyTextAttrs(textLabels,Y,xScale,yScale,rScale)
	},
	resetScales : function(dataset,xScale,yScale,rScale){
		xScale
			.domain([d3.min(dataset, function(d) { return d[0]; }), d3.max(dataset, function(d) { return d[0]; })])
			.range([padding, w - padding]);
		yScale
			.domain([d3.min(dataset, function(d) { return d[1]; }), d3.max(dataset, function(d) { return d[1]; })])
			.range([h-padding,padding]);
		rScale
			.domain([d3.min(dataset, function(d) { return d[1]; }), d3.max(dataset, function(d) { return d[1]; })])
			.range([2, 5]);
	},
	resetAxis : function(xAxis,yAxis,xScale,yScale){
		xAxis = d3.axisBottom(xScale)
		yAxis = d3.axisLeft(yScale)
	},
	redrawAxis : function(svg,xAxis,yAxis){
		svg.select('.x.axis')
			.transition()
		    .call(xAxis);

		svg.select('.y.axis')
			.transition()
		    .call(yAxis);
	},
	applyImageAttrs : function(images,products,Y,xScale,yScale,rScale){
		images
			.transition()
			.attr("xlink:href",function(d,i) {
				return products[i]['ImageUrl'];
			})
			.attr("x",function(d,i) {
				return xScale(Y[i][0]);
			})
			.attr("y",function(d,i) {
				return yScale(Y[i][1]);
			})
			.attr("width", function(d,i) {
				return 50;
			})
			.attr("height", function(d,i) {
				return 50;
			})

		images
			.on("mouseover",function(d,i){				
				var xPosition = parseFloat(d3.select(this).attr("x")) * 1.5 ;
				var yPosition = parseFloat(d3.select(this).attr("y"))/2 + h / 2;

				var svg = d3.select('#tooltip')
				//Update the tooltip position and value

				d3.select(".product_image")
				  .attr("src",d.ImageUrl);

				d3.select("#tooltip")
					.attr("class","ui card")
					.style("left", xPosition + "px")
					.style("top", yPosition + "px")
					.select(".product_name")
				  	.text(d.Name);				

				d3.select("#tooltip")
				  .select(".product_price")
				  .text("$"+d.Price);

				//Show the tooltip
				d3.select("#tooltip").classed("hidden", false);
			})
			.on("mouseout",function(d,i){
				var tooltip = d3.select('#tooltip')
				tooltip.attr("class","hidden")
			})
			.on("click",function(d,i){
				var tooltip = d3.select('#tooltip')
				tooltip.attr("class","hidden")
				console.log(d)
				components.visual.setState({
					count: 1,
					feature_vector_url : "/getSimilarProducts",
					categories_checked: [],
					product_id_to_search: d.Id
				})
				components['vertical-menu'].resetCategories()
				components.visual.resetParameters();
				components.visual.startVisualizationProcessing()	
				components.visual.resetMessageViews()
				width = w = $('.ten.wide.column').width() * 0.9
				d3.select('svg').attr('width',w)	
			})
	},
	applyCircleAttrs : function(circles,Y,xScale,yScale,rScale){
		circles
			.transition()
			.attr("cx",function(d,i) {
				return xScale(Y[i][0]);
			})
			.attr("cy",function(d,i) {
				return yScale(Y[i][1]);
			})
			.attr("r", function(d,i) {
				return rScale(Y[i][1]);
			})

		circles
			.on("mouseover",function(d,i){
				var svg = d3.select('svg')
				svg
					.append('text')
					.attr('x',xScale(Y[i][0]))
					.attr('y',yScale(Y[i][1]))
					.text(d)
					.attr("font-family", "sans-serif")
					.attr("font-size", "3em")
					.attr("fill", "pink");

			})
			.on("mouseout",function(d,i){
				var svg = d3.select('svg')
				svg
					.select('text')
					.remove()

			})
	},
	applyTextAttrs : function(textLabels,Y,xScale,yScale,rScale){
		textLabels
			.transition()
			.attr("x", function(d,i) {
				return xScale(Y[i][0]);
			})
			.attr("y", function(d,i) {
				return yScale(Y[i][1]);
			})
			.text(function(d){
				// return "("+d[0]+","+d[1]+")"
				return d
			})
			.attr("font-family", "sans-serif")
			.attr("font-size", "11px")
			.attr("fill", "red");
	},
	render: function(){
		return(
			<div>						
				Number of iterations: {this.state.count}/{this.state.max_iterations}
			</div>
		)
	}
})

var Footer = React.createClass({
	componentDidMount: function(){
	},
	render: function(){
		classes = "ui "+ this.props.className
		return( 
		  <div className={classes} ref="footerSection"> 		  	
		  	<div className="ui text container center aligned">
		  		<DividerBlock quantity="1"/><br/>      
		  		<p>Built with <a href="https://nodejs.org/en/">NodeJS</a>, <a href="https://facebook.github.io/react/index.html">ReactJS</a> and <a href="http://semantic-ui.com/">SemanticUI</a></p> 
		  		<div className="ui images">
		  			<a href="https://nodejs.org/en/"><img className="ui centered mini image" src="/images/nodejs.png"/></a>
				  	<a href="https://facebook.github.io/react/index.html"><img className="ui centered mini image" src="/images/reactjs.svg"/></a>
		  			<a href="https://semantic-ui.com/"><img className="ui centered mini circular image" src="/images/semantic.png"/></a>
				</div>		  		
		  		<p>&copy; 2016 Amogh Param<br/>Seattle, WA</p>
		  		<DividerBlock quantity="2"/>        
		  	</div>    		  	
		  </div>
		);
	}
})



ReactDOM.render(
	<MenuBar />,
	document.getElementById('top-menu-container')
)

ReactDOM.render(
	<VerticalMenu/>,
	document.getElementById('vertical-menu')
)

ReactDOM.render(
	<Visual />,
	document.getElementById('content')
)