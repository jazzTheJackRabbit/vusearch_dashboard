#!flask/bin/python
from flask import Flask, jsonify, abort, make_response, request, url_for, render_template
import requests
import re
import pandas as pd
from StringIO import StringIO
import json
import os
import random
app = Flask(__name__)
BASE_URL = "https://72vq364axb.execute-api.us-east-1.amazonaws.com/gamma/"

def url_join(path):
	return BASE_URL + str(path)

########### REST API ###########
# Get all listings
@app.route('/', methods=['GET'])
def index():
	return render_template('dashboard.html')

@app.route('/d3', methods=['GET'])
def d3():
	return render_template('d3.html')

@app.route('/getRandomFeatureVectors',methods=['GET'])
def getRandomFeatureVectors():
	url = url_join("categories/1/random")
	headers = {
		'X-API-Key': 'a4f9Zc0OBr3q3pS4yuXEA4hw4HnLBgHP4HbkycRo',
	}
	response = requests.get(url,headers=headers)
	json_response = response.json()
	return jsonify(json_response)

@app.route('/getSimilarProducts',methods=['POST'])
def getSimilarProducts():
	product = (dict(request.json)['product'])
	url = url_join("products/{0}/similar").format(product)
	headers = {
		'X-API-Key': 'a4f9Zc0OBr3q3pS4yuXEA4hw4HnLBgHP4HbkycRo',
	}
	response = requests.get(url,headers=headers)
	json_response = response.json()
	return jsonify(json_response)

@app.route('/getCategoryFeatureVectors',methods=['POST'])
def getCategoryFeatureVectors():
	categories = (dict(request.json)['categories_checked'])
	print(categories)
	return_response = {"results":[]}
	for category in categories:
		url = url_join("categories/{0}/random").format(category)
		headers = {
			'X-API-Key': 'a4f9Zc0OBr3q3pS4yuXEA4hw4HnLBgHP4HbkycRo',
		}
		response = requests.get(url,headers=headers)
		json_response = response.json()
		return_response['results'].extend(json_response['results'])

	random_sample_list = random.sample(xrange(len(return_response['results'])),100)
	
	return_response['results'] = [return_response['results'][i] for i in random_sample_list]
	return jsonify(return_response)

def getCategoryFeatureVectorsRandom():
	categories = (dict(request.json)['categories_checked'])
	return_response = {"results":[]}
	
	data = {
		'CategoryIds': categories,
		'Count': 100
	}

	url = url_join("vuSearch_MultipleRandomPick")

	headers = {
		'X-API-Key': 'a4f9Zc0OBr3q3pS4yuXEA4hw4HnLBgHP4HbkycRo',
	}
	response = requests.get(url,headers=headers,data=data)
	json_response = response.json()
	# return_response['results'].extend(json_response['results'])
	# random_sample_list = random.sample(xrange(len(return_response['results'])),100)
	# return_response['results'] = [return_response['results'][i] for i in random_sample_list]

	return jsonify(json_response)

@app.route('/getProducts',methods=['POST'])
def getProducts():
	url = url_join("products")
	headers = {
		'X-API-Key': 'a4f9Zc0OBr3q3pS4yuXEA4hw4HnLBgHP4HbkycRo',
	}
	data = dict(request.form).keys()[0]
	response = requests.post(url,headers=headers,data=data)
	json_response = response.json()
	return jsonify(json_response)

@app.route('/listCategories',methods=['GET'])
def listCategories():
	categoryFile = open("./src/categories.txt")
	categories = categoryFile.read().split("\n")
	del categories[-1]
	return jsonify(categories)

############ END REST API ###########
if __name__ == '__main__':
	app.run(debug=True)
