var animatationDuration = 500
var width = w = $("#myDiv").width()
var height = h = 600
var barSpace = 5
var padding = 50 

$(document).on("ready",function(){
	
	// Dataset
	var dataset = [];
	dataset = generateNewData()

	// Setup scale functions
	var xScale = d3.scaleLinear()
	var yScale = d3.scaleLinear()
	var rScale = d3.scaleLinear()

	resetScales(dataset,xScale,yScale,rScale)

	// Drawing code 
	var svg = d3.select('#myDiv')
				.append('svg')
				.attr('width',w)
				.attr('height',h)
				.attr('style','overflow:overlay')

	// Scatter plot dots/circle layer
	var circles_layer = svg.selectAll('circle')
		.data(dataset)
		.enter()
			.append('circle')
	
	applyCircleAttrs(circles_layer,xScale,yScale,rScale)

	// Scatter plot text layer
	// var text_labels_layer = svg.selectAll('text')
	// 	.data(dataset)
	// 	.enter()
	// 		.append('text')

	// applyTextAttrs(text_labels_layer,xScale,yScale,rScale)

	// Axis layer
	// X-axis
	// var xAxis = d3.axisBottom(xScale)
 //  	svg.append("g")
	//   	.attr("class", "x axis") 
	//   	.attr("transform", "translate(" + 0 + "," + (h-padding/2) + ")")
	//     .call(xAxis);

 //    // Y-axis
	// var yAxis = d3.axisLeft(yScale)
 //    svg.append("g")
	//   	.attr("class", "y axis") 
	//   	.attr("transform", "translate(" + padding/1.5 + ",0)")
	//     .call(yAxis);


	intervalID = setInterval(function(){
		if(count < max_iterations){
			tsne.step()
			this.plot_data(tsne.getSolution())
			// count++
			this.setState({
				count: count++
			})
		}	
		else{
			clearInterval(intervalID)
		}	
	}.bind(this),0.01)

	setInterval(function(){		
		dataset = generateNewData()
		resetScales(dataset,xScale,yScale,rScale)			
		
		var circles_layer = svg.selectAll('circle')
			.data(dataset)

		applyCircleAttrs(circles_layer,xScale,yScale,rScale)

		// resetAxis(xAxis, yAxis, xScale, yScale)
		// redrawAxis(svg,xAxis,yAxis)
	},1000000)
})


var generateNewData = function(){
	var dataset = []
	var numDataPoints = 50 + Math.round((Math.random() * 100));
	var xRange = Math.random() * 1000;
	var yRange = Math.random() * 1000;
	for (var i = 0; i < numDataPoints; i++) {
	    var newNumber1 = Math.floor(Math.random() * xRange);
	    var newNumber2 = Math.floor(Math.random() * yRange);
	    dataset.push([newNumber1, newNumber2]);
	}
	return dataset
}

var resetScales = function(dataset,xScale,yScale,rScale){
	xScale
		.domain([0, d3.max(dataset, function(d) { return d[0]; })])
		.range([padding, w - padding]);
	yScale
		.domain([0, d3.max(dataset, function(d) { return d[1]; })])
		.range([h-padding,padding]);
	rScale
		.domain([0, d3.max(dataset, function(d) { return d[1]; })])
		.range([2, 5]);
}

var resetAxis = function(xAxis,yAxis,xScale,yScale){
	xAxis = d3.axisBottom(xScale)
	yAxis = d3.axisLeft(yScale)
}

var redrawAxis = function(svg,xAxis,yAxis){
	svg.select('.x.axis')
		.transition()
	    .call(xAxis);

	svg.select('.y.axis')
		.transition()
	    .call(yAxis);
}

var applyCircleAttrs = function(circles,xScale,yScale,rScale){
	circles
		.transition()
		.attr("cx",function(d){
			return xScale(d[0])
		})
		.attr("cy",function(d){
			return yScale(d[1])
		})
		.attr("r", function(d) {
			return rScale(d[1]);
		});
}

var applyTextAttrs = function(textLabels,xScale,yScale,rScale){
	textLabels
		.attr("x", function(d) {
			return xScale(d[0]);
		})
		.attr("y", function(d) {
			return yScale(d[1]);
		})
		.text(function(d){
			return "("+d[0]+","+d[1]+")"
		})
		.attr("font-family", "sans-serif")
		.attr("font-size", "11px")
		.attr("fill", "red");
}