// Radians are the ratio of the circumference to the diameter - always a constant for every circle.

var animatationDuration = 500
var width = w = 300
var height = h = 300
var barSpace = 5
var padding = 50 

$(document).on("ready",function(){	
	var dataset = [ 5, 10, 20, 45, 6, 25 ];
	var pie = d3.pie();

	var outerRadius = w / 2;
	var innerRadius = w/3;
	var arc = d3.arc()
	                .innerRadius(innerRadius)
	                .outerRadius(outerRadius);

	console.log(pie(dataset))

    var svg = d3.select("#content")
            .append("svg")
            .attr("width", w)
            .attr("height", h);


    var arcs = svg.selectAll("g.segment")
        .data(pie(dataset))
        .enter()
	        .append("g")
		        .attr("class", "segment")
		        .attr("transform", "translate(" + outerRadius + ", " + outerRadius + ")")
			.append("path")
		    .attr("fill", function(d, i) {
		        return d3.schemeCategory10[i];
		    })
		    .attr("d", arc)

	svg.selectAll("g.segment")
		.append("text")
	    .attr("transform", function(d) {
	    	return "translate(" + arc.centroid(d) + ")";
	    })
	    .attr("text-anchor", "middle")
	    .text(function(d) {
	    	return d.value;
	    });
});