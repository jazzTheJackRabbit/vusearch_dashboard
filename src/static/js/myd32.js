$(document).on("ready",function(){

	// Constants
	var w = $("#myDiv").width()
	var h = 500
	var barPadding = 50
	
	// Dataset
	var dataset = [];
	var numDataPoints = 50;
	var xRange = Math.random() * 1000;
	var yRange = Math.random() * 1000;
	for (var i = 0; i < numDataPoints; i++) {
	    var newNumber1 = Math.floor(Math.random() * xRange);
	    var newNumber2 = Math.floor(Math.random() * yRange);
	    dataset.push([newNumber1, newNumber2]);
	}

	// Setup scale functions
	var xScale = d3.scaleLinear()
								 .domain([0, d3.max(dataset, function(d) { return d[0]; })])
								 .range([barPadding, w - barPadding]);
	var yScale = d3.scaleLinear()
						 .domain([0, d3.max(dataset, function(d) { return d[1]; })])
						 .range([h - barPadding, barPadding]);
	var rScale = d3.scaleLinear()
	                     .domain([0, d3.max(dataset, function(d) { return d[1]; })])
	                     .range([2, 5]);


	// Drawing code 
	var svg = d3.select('#myDiv')
				.append('svg')
				.attr('width',w)
				.attr('height',h)

	// Scatter plot dots/circle layer
	var circles_layer = svg.selectAll('circle')
	.data(dataset)
	.enter()
		.append('circle')
			.attr("cx",function(d){
				return xScale(d[0])
			})
			.attr("cy",function(d){
				return yScale(d[1])
			})
			.attr("r", function(d) {
				return rScale(d[1]);
			});

	// Scatter plot text layer
	var text_labels_layer = svg.selectAll('text')
	.data(dataset)
	.enter()
		.append('text')
			.attr("x", function(d) {
					return xScale(d[0]);
			})
			.attr("y", function(d) {
					return yScale(d[1]);
			})
			.text(function(d){
				return "("+d[0]+","+d[1]+")"
			})
			.attr("font-family", "sans-serif")
			.attr("font-size", "11px")
			.attr("fill", "red");

	// Axis layer
	// X-axis
	var xAxis = d3.axisBottom(xScale).ticks(5);  
  	svg.append("g")
  	.attr("class", "axis") 
	.attr("transform", "translate(0," + (h - barPadding) + ")")
    .call(xAxis);

    // Y-axis
	var yAxis = d3.axisLeft(yScale).ticks(5)
    svg.append("g")
  	.attr("class", "axis") 
  	.attr("transform", "translate("+ barPadding * 0.9 + ",0)")
    .call(yAxis);
})