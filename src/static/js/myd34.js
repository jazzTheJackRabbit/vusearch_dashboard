var animatationDuration = 500
var width = w = 300
var height = h = 300
var barSpace = 5
var padding = 50 


$(document).on("ready",function(){	
	var dataset = [
	        { apples: 5, oranges: 10, grapes: 22 },
	        { apples: 4, oranges: 12, grapes: 28 },
	        { apples: 2, oranges: 19, grapes: 32 },
	        { apples: 7, oranges: 23, grapes: 35 },
	        { apples: 23, oranges: 17, grapes: 43 }
	];

	var stack = d3.stack()
					.keys(["apples","oranges","grapes"])
					.order(d3.stackOrderNone)
					.offset(d3.stackOffsetNone)

	var series = stack(dataset)

	var xScale = 
		d3.scaleBand()

	var yScale = 
		d3.scaleLinear()

	// yScale for the actual values
	var hScale = 
		d3.scaleLinear()
	
	resetScales(dataset,xScale, yScale, hScale)

	var rectangles = 
		svg.selectAll("rect")
		.data(function(d){return d})
		.enter()
			.append("rect")	

	applyBarAttrs(rectangles, xScale, yScale, hScale)
});

var resetScales = function(dataset,xScale,yScale,hScale){
	xScale
		.domain(d3.range(dataset[0].length))
		.rangeRound([0,w])

	yScale
		.domain([0,d3.max(dataset,function(d){return d.value})])
		.range([h-padding,padding])

	// yScale for the actual values
	hScale
		.domain([0,d3.max(dataset,function(d){return d.value})])
		.range([0,h-2*padding])
}

var resetAxis = function(xAxis,yAxis,xScale,yScale){
	xAxis = d3.axisBottom(xScale)
    yAxis = d3.axisLeft(yScale)
}

var redrawTextLabels = function(textLabels,xScale,yScale,hScale){
	applyTextAttrs(textLabels,xScale,yScale,hScale)
}

var applyTextAttrs = function(elements,xScale,yScale,hScale){
	var fontSize = xScale.bandwidth() * 0.9
	elements
		.transition()
		.text(function(d) {
			return d.value;
		})
		.attr("x",function(d,i){
			return xScale(i) + xScale.bandwidth()/2
		})
		.attr("y",function(d){
			return yScale(d.value) + fontSize + barSpace
		})
		.attr("fill",function(d){
			 return "rgb(150, " + (d.value * 2) + ",150)";
		})
		.attr("font-size",function(d){
			return fontSize
		})
		.attr("text-anchor", "middle")
		.attr("class","value")
		.style("pointer-events",'none')
}

var redrawBars = function(bars,xScale,yScale,hScale){
	applyBarAttrs(bars,xScale,yScale,hScale)
}

var applyBarAttrs = function(elements,xScale,yScale,hScale){
	console.log(elements)
	elements
		.transition()
		.attr("x",function(d,i){
			return xScale(i)
		})		
		.attr("y",function(d){
			return yScale(d)
		})
		.attr("width",function(d){
			return xScale.bandwidth()
		})
		.attr("height",function(d){
			return hScale(d.value) 
		})
		.attr("fill", function(d) {   // <-- Down here!
			return "rgb(150, 250, " + (d.value * 10) + ")";
		})
}

var redrawAxis = function(svg,xAxis,yAxis){
	svg.select('g')
		.filter('.x.axis')
	    .transition()
	    .call(xAxis)

    svg.select('.y.axis')
	    .transition()
	    .call(yAxis)
}