var animatationDuration = 500
var width = w = 600
var height = h = 600
var barSpace = 5
var padding = 50 

$(document).on("ready",function(){	
	var graph = {
        nodes: [
                { name: "Adam" },
                { name: "Bob" },
                { name: "Carrie" },
                { name: "Donovan" },
                { name: "Edward" },
                { name: "Felicity" },
                { name: "George" },
                { name: "Hannah" },
                { name: "Iris" },
                { name: "Jerry" }
        ],
        links: [
                { source: 0, target: 1 },
                { source: 0, target: 2 },
                { source: 0, target: 3 },
                { source: 0, target: 4 },
                { source: 1, target: 5 },
                { source: 2, target: 5 },
                { source: 2, target: 5 },
                { source: 3, target: 4 },
                { source: 5, target: 8 },
                { source: 5, target: 9 },
                { source: 6, target: 7 },
                { source: 7, target: 8 },
                { source: 8, target: 9 }
        ]
	};

	var canvas = document.querySelector("canvas"),
    context = canvas.getContext("2d"),
    width = canvas.width,
    height = canvas.height;

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }))
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(width / 2, height / 2));


	  simulation
	      .nodes(graph.nodes)
	      .on("tick", ticked);

	  simulation.force("link")
	      .links(graph.links);

	  d3.select(canvas)
	      .call(d3.drag()
	          .container(canvas)
	          .subject(dragsubject)
	          .on("start", dragstarted)
	          .on("drag", dragged)
	          .on("end", dragended));

	  function ticked() {
	    context.clearRect(0, 0, width, height);

	    context.beginPath();
	    graph.links.forEach(drawLink);
	    context.strokeStyle = "#aaa";
	    context.stroke();

	    context.beginPath();
	    graph.nodes.forEach(drawNode);
	    context.fill();
	    context.strokeStyle = "#fff";
	    context.stroke();
	  }

	  function dragsubject() {
	    return simulation.find(d3.event.x, d3.event.y);
	  }

	function dragstarted() {
	  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
	  d3.event.subject.fx = d3.event.subject.x;
	  d3.event.subject.fy = d3.event.subject.y;
	}

	function dragged() {
	  d3.event.subject.fx = d3.event.x;
	  d3.event.subject.fy = d3.event.y;
	}

	function dragended() {
	  if (!d3.event.active) simulation.alphaTarget(0);
	  d3.event.subject.fx = null;
	  d3.event.subject.fy = null;
	}

	function drawLink(d) {
	  context.moveTo(d.source.x, d.source.y);
	  context.lineTo(d.target.x, d.target.y);
	}

	function drawNode(d) {
	  context.moveTo(d.x + 3, d.y);
	  context.arc(d.x, d.y, 3, 0, 2 * Math.PI);
	}


});