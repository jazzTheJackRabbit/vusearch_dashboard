var animatationDuration = 500
var width = w = $("#myDiv").width()
var height = h = 600
var barSpace = 5
var padding = 50 
$(document).on("ready",function(){	

	var dataset = [ { key: 0, value: 5 },
                { key: 1, value: 10 },
                { key: 2, value: 13 },
                { key: 3, value: 19 },
                { key: 4, value: 21 },
                { key: 5, value: 25 },
                { key: 6, value: 22 },
                { key: 7, value: 18 },
                { key: 8, value: 15 },
                { key: 9, value: 13 },
                { key: 10, value: 11 },
                { key: 11, value: 12 },
                { key: 12, value: 15 },
                { key: 13, value: 20 },
                { key: 14, value: 18 },
                { key: 15, value: 17 },
                { key: 16, value: 16 },
                { key: 17, value: 18 },
                { key: 18, value: 23 },
                { key: 19, value: 25 } ];

	var svg = 
		d3.select("#myDiv")
	    .append("svg")
	    .attr("width", width)
	    .attr("height", height)
	    .style("overflow","overlay")

	var key = function(d){
		return d.key
	}

	// Scale functions
	// xScale will be used for the index and not the actual values
	var xScale = 
		d3.scaleBand()

	var yScale = 
		d3.scaleLinear()

	// yScale for the actual values
	var hScale = 
		d3.scaleLinear()
	
	resetScales(dataset,xScale, yScale, hScale)

	// Rectangle layer
	bar_width_with_padding = width/(dataset.length) - (2*barSpace)
	var rectangles = 
		svg.selectAll("rect")
		.data(dataset,key)
		.enter()
			.append("rect")	

	// Text layer
	var fontSize = bar_width_with_padding/1.5
	var text_labels = 
		svg.selectAll("text.value")
		.data(dataset,key)
		.enter()
			.append("text")
	
	applyBarAttrs(rectangles,xScale,yScale,hScale)	
	applyTextAttrs(text_labels,xScale,yScale,hScale)
	
	rectangles.on("click",function(){
		sortBars(xScale,yScale,hScale)
		sortTextLabels(xScale,yScale,hScale)
	})

	rectangles.on("mouseover",function(d){
		var xPosition = parseFloat(d3.select(this).attr('x')) + xScale.bandwidth()/2
		var yPosition = parseFloat(d3.select(this).attr('y'))

		svg.append("text")
		  .attr("id", "tooltip")
		  .attr("x", xPosition)
		  .attr("y", yPosition)
		  .attr("text-anchor", "middle")
		  .attr("font-family", "sans-serif")
		  .attr("font-size", xScale.bandwidth() * 0.9)
		  .attr("font-weight", "bold")
		  .attr("fill", "black")
		  .attr("class","tooltip")
		  .text(d.value);


		 console.log(d)
	});

	rectangles.on("mouseout",function(d){
		var xPosition = parseFloat(d3.select(this).attr('x')) + xScale.bandwidth()/2
		var yPosition = parseFloat(d3.select(this).attr('y'))

		svg.selectAll("text.tooltip").remove()
	});

	var xAxis = d3.axisBottom(xScale)
    svg.append('g')
		.attr("class","x axis")
		.attr("transform", "translate(0," + (h - padding) + ")")
		.call(xAxis)


    var yAxis = d3.axisLeft(yScale)
    svg.append('g')
	    .attr("class","y axis")
	    .attr("transform", "translate(" + padding/2 + ",0)")
	    .call(yAxis)

    // ***************
    // ON CLICK
	// ***************

	d3.select('.add')
    .on("click", function() {

        maxValue = Math.round(Math.random() * 100)        

		var newNumber = Math.floor(Math.random() * maxValue);
		var lastKey = dataset[dataset.length-1].key
		dataset.push({key:lastKey + 1,value: newNumber});
		
       	resetScales(dataset,xScale,yScale,hScale)	    
	    resetAxis(xAxis,yAxis,xScale,yScale)

        //Update all rects
        var bars = svg.selectAll("rect")
   			.data(dataset,key)

        var textLabels = svg.selectAll('text')
			.data(dataset,key)
		
		// Current bars
   		redrawBars(bars,xScale,yScale,hScale)

		// New bars
		applyBarAttrs(bars
			.enter() //Runs only for the new data
				.append("rect"),xScale,yScale,hScale)

		// Current text labels
		redrawTextLabels(textLabels,xScale,yScale,hScale)
		applyTextAttrs(textLabels
						.enter() //Runs only for the new data
							.append("text"),xScale,yScale,hScale)
		redrawAxis(svg,xAxis,yAxis)

		bars.on("click",function(){
			sortBars(xScale,yScale,hScale)
			sortTextLabels(xScale,yScale,hScale)
		})

		bars.on("mouseover",function(d){
			var xPosition = parseFloat(d3.select(this).attr('x')) + xScale.bandwidth()/2
			var yPosition = parseFloat(d3.select(this).attr('y'))

			svg.append("text")
			  .attr("id", "tooltip")
			  .attr("x", xPosition)
			  .attr("y", yPosition)
			  .attr("text-anchor", "middle")
			  .attr("font-family", "sans-serif")
			  .attr("font-size", xScale.bandwidth() * 0.9)
			  .attr("font-weight", "bold")
			  .attr("fill", "black")
			  .attr("class","tooltip")
			  .text(d.value);


			 console.log(d)
		});

		bars.on("mouseout",function(d){
			var xPosition = parseFloat(d3.select(this).attr('x')) + xScale.bandwidth()/2
			var yPosition = parseFloat(d3.select(this).attr('y'))

			svg.selectAll("text.tooltip").remove()
		});
    });

    d3.select('.remove')
    .on("click", function() {

    	dataset.shift()	    

    	var bars = d3.selectAll('rect').data(dataset,key)
    	var textLabels = d3.selectAll('text.value').data(dataset,key)

    	bars.exit().remove();
	    textLabels.exit().remove()

	    resetScales(dataset,xScale,yScale,hScale)	    
	    resetAxis(xAxis,yAxis,xScale,yScale)

	    redrawAxis(svg,xAxis,yAxis)
	    redrawBars(bars,xScale,yScale,hScale)
	    redrawTextLabels(textLabels,xScale,yScale,hScale)

	    });
})

var sortBars = function(xScale,yScale,hScale){

     applyBarAttrs(d3.select('svg').selectAll("rect")
           .sort(function(a, b) {
                 return d3.ascending(a.value, b.value);
           }),xScale,yScale,hScale)
};

var sortTextLabels = function(xScale,yScale,hScale){
	applyTextAttrs(d3.select('svg').selectAll("text.value")
           .sort(function(a, b) {
                 return d3.ascending(a.value, b.value);
           }),xScale,yScale,hScale)
}

var resetScales = function(dataset,xScale,yScale,hScale){
	xScale
		.domain(d3.range(dataset.length))
		.rangeRound([0,w])
		.paddingInner([0.3])
		.paddingOuter([1])

	yScale
		.domain([0,d3.max(dataset,function(d){return d.value})])
		.range([h-padding,padding])

	// yScale for the actual values
	hScale
		.domain([0,d3.max(dataset,function(d){return d.value})])
		.range([0,h-2*padding])
}

var resetAxis = function(xAxis,yAxis,xScale,yScale){
	xAxis = d3.axisBottom(xScale)
    yAxis = d3.axisLeft(yScale)
}

var redrawTextLabels = function(textLabels,xScale,yScale,hScale){
	applyTextAttrs(textLabels,xScale,yScale,hScale)
}

var applyTextAttrs = function(elements,xScale,yScale,hScale){
	var fontSize = xScale.bandwidth() * 0.9
	elements
		.transition()
		.text(function(d) {
			return d.value;
		})
		.attr("x",function(d,i){
			return xScale(i) + xScale.bandwidth()/2
		})
		.attr("y",function(d){
			return yScale(d.value) + fontSize + barSpace
		})
		.attr("fill",function(d){
			 return "rgb(150, " + (d.value * 2) + ",150)";
		})
		.attr("font-size",function(d){
			return fontSize
		})
		.attr("text-anchor", "middle")
		.attr("class","value")
		.style("pointer-events",'none')
}

var redrawBars = function(bars,xScale,yScale,hScale){
	applyBarAttrs(bars,xScale,yScale,hScale)
}

var applyBarAttrs = function(elements,xScale,yScale,hScale){
	console.log(elements)
	elements
		.transition()
		.attr("x",function(d,i){
			return xScale(i)
		})		
		.attr("y",function(d){
			return yScale(d.value)
		})
		.attr("width",function(d){
			return xScale.bandwidth()
		})
		.attr("height",function(d){
			return hScale(d.value) 
		})
		.attr("fill", function(d) {   // <-- Down here!
			return "rgb(150, 250, " + (d.value * 10) + ")";
		})
}

var redrawAxis = function(svg,xAxis,yAxis){
	svg.select('g')
		.filter('.x.axis')
	    .transition()
	    .call(xAxis)

    svg.select('.y.axis')
	    .transition()
	    .call(yAxis)
}