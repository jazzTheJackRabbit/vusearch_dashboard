(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
var tsnejs = require('./tsne.js')

var components = {}

var _defaults = {
	'feature_vector_url': '/getCategoryFeatureVectors',
	'categories': [],
	'categories_checked': [1],
	'product_id_to_search': "",
	'tsne_parameters':{
		'epsilon': 10,
		'perplexity': 30,
		'dim': 2,
		'max_iterations': 1000,
	}
}

var MenuBar = React.createClass({displayName: "MenuBar",
	render: function(){
		return(
			React.createElement("div", {className: "ui top inverted fixed borderless menu topmenu"}, 
			  React.createElement("a", {className: "item", href: "/"}, 
			    	React.createElement("img", {src: "static/images/logo.png", className: "ui avatar image"})
			  )
			)
		)
	}
})

var CategoryFields = React.createClass({displayName: "CategoryFields",
	getInitialState: function(){
		return({
			checked: false
		})
	},
	componentDidMount: function(){
		if(components['visual']){
			if (components['visual'].state['categories_checked'].indexOf(this.props.index)!= -1){
				this.setState({
					checked: true
				})
			}
		}
		else{
			if (_defaults['categories_checked'].indexOf(this.props.index)!= -1){
				this.setState({
					checked: true
				})
			}
		}
	},
	onChange: function(){
		this.setState({
			checked: !this.state.checked
		})
	},
	render: function(){		
		return(
			React.createElement("div", {className: "field"}, 
				React.createElement("div", {className: "ui checkbox"}, 
					React.createElement("input", {type: "checkbox", name: "category", value: this.props.index, checked: this.state.checked, onChange: this.onChange}), 
				  React.createElement("label", null, this.props.name)
				)
			)
		)
	}
})

var Message = React.createClass({displayName: "Message",
	render: function(){
		return(
		React.createElement("div", {className: "ui mini message no-padding"}, 
		  "This is a mini message."
		)
		)
	}
})

var TSNEFields = React.createClass({displayName: "TSNEFields",
	render: function(){
		return(
			React.createElement("div", {className: "field"}, 
				React.createElement("div", {className: "ui fluid labeled input"}, 
				  React.createElement("div", {className: "ui label"}, 
				    this.props.name
				  ), 
				  React.createElement("input", {type: "text", placeholder: this.props.value})
				)
			)
		)
	}
})

var Loader = React.createClass({displayName: "Loader",
	getInitialState: function(){
		return({
			"hidden": false
		})
	},
	render: function(){
		return(
			React.createElement("div", {class: this.state.hidden ? "" : "ui loader"})
		)
	}
})

var VerticalMenu = React.createClass({displayName: "VerticalMenu",
	getInitialState: function(){
		return({
			categories: _defaults['categories'],
			categories_checked: _defaults['categories_checked'],
			tsne_parameters: _defaults['tsne_parameters']
		})
	},
	handleSubmit: function(){		
		components.visual.setState({
			count: 1,
			feature_vector_url : "/getCategoryFeatureVectors",
		})
		components['vertical-menu'].updateTSNEParameters();				
		

		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		let categoriesChecked = []
		for (let i=0; i<checkboxes.length; i++){
			if (checkboxes[i].checked){
				categoriesChecked.push(i)
			}			
		}
		components['vertical-menu'].setState({
			categories_checked : categoriesChecked
		})
		components['visual'].setState({
			categories_checked : categoriesChecked
		})

		console.log(categoriesChecked)
		if(categoriesChecked.length){			
			components.visual.resetParameters();
			components.visual.startVisualizationProcessing()	
			components.visual.resetMessageViews()
			width = w = $('.ten.wide.column').width() * 0.9
			d3.select('svg').attr('width',w)		
		}
		else{
			components.visual.showNoCategorySelected();
		}
	},
	resetCategories(){
		// Update Categories
		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		for (let i=0; i<checkboxes.length; i++){
			checkboxes[i].checked = false		
		}
		components['vertical-menu'].setState({
			categories_checked : []
		})
		components['visual'].setState({
			categories_checked : []
		})
	},
	resetCategories(){
		// Update Categories
		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		categoriesChecked = []
		for (let i=0; i<checkboxes.length; i++){
			checkboxes[i].checked = false		
		}
	},
	updateCategories(){
		// Update Categories
		category_fields = this.refs['category_fields']
		category_update ={}
		
		let checkboxes = document.querySelectorAll('input[name=category]')
				
		categoriesChecked = []
		for (let i=0; i<checkboxes.length; i++){
			if (checkboxes[i].checked){
				categoriesChecked.push(i)
			}			
		}
		this.setState({
			categories_checked : categoriesChecked
		})
		components['visual'].setState({
			categories_checked : categoriesChecked
		})
	},
	updateTSNEParameters(){
		// Update TSNE Parameters
		tsne_parameter_fields = this.refs['tsne_parameter_fields']
		tsne_parameters_update ={}
		for (let i=0; i<tsne_parameter_fields.childNodes.length; i++){
			parameter_name = d3.select(tsne_parameter_fields.childNodes[i]).select('div.ui.label').property('innerText')
			parameter_value = d3.select(tsne_parameter_fields.childNodes[i]).select('input').property('value') ? d3.select(tsne_parameter_fields.childNodes[i]).select('input').property('value') :  d3.select(tsne_parameter_fields.childNodes[i]).select('input').property('placeholder')  
			tsne_parameters_update[parameter_name] = parameter_value
		}
		components['visual'].setState({
			tsne_parameters: tsne_parameters_update
		})
	},
	componentDidMount: function(){
		$('.ui.accordion')
		  .accordion('open',0)
		;
		components['vertical-menu'] = this;		
		$.ajax({
			url: "/listCategories",
			method: 'GET',
			success: function(response){
				categories = response.map(function(category){
					category = category.split("-").join(" ")
					return toTitleCase(category)
				})

				components['vertical-menu'].setState({
					categories : categories
				})
			}
		});
	},
	render: function(){
		return(
			React.createElement("div", {className: "ui big vertical accordion menu"}, 
			  React.createElement("div", {className: "item"}, 
			    React.createElement("a", {className: "title"}, 
			      React.createElement("i", {className: "block layout icon"}), 
			      "Choose Categories", 
			      React.createElement("i", {className: "dropdown icon"})
			    ), 			    
			    React.createElement("div", {className: "content vertical-menu"}, 
			      React.createElement("div", {className: "ui form"}, 
			        React.createElement("div", {className: "grouped fields", ref: "category_fields"}, 			        			        
			          
			          	this.state.categories.map(function(component,index){
			          		return React.createElement(CategoryFields, {name: component, index: index})
			          	})
			          		          
			        )
			      )
			    )
			  ), 	
			  React.createElement("div", {className: "item"}, 
			    React.createElement("a", {className: "title"}, 
			    	React.createElement("i", {className: "setting icon"}), 
			      	"TSNE Parameters", 
			      	React.createElement("i", {className: "dropdown icon"})
			    ), 			    
			    React.createElement("div", {className: "content vertical-menu"}, 
			      React.createElement("div", {className: "ui form"}, 
			        React.createElement("div", {className: "grouped fields", ref: "tsne_parameter_fields"}, 			        			        
			          
			          	Object.keys(this.state.tsne_parameters).map(function(tsne_parameter,index){
			          		return React.createElement(TSNEFields, {name: tsne_parameter, value: this.state.tsne_parameters[tsne_parameter]})
			          	}.bind(this))
			          		          
			        )
			      )
			    )
			  ), 		  
			  React.createElement("div", {className: "fluid ui inverted green button item", onClick: this.handleSubmit, ref: "update_button"}, 
			  	"Update"
			  )
			)			
		)
	}
})

var animatationDuration = 500
var width = w = $("#myDiv").width()
var height = h = 600
var barSpace = 5
var padding = 50 
var Visual = React.createClass({displayName: "Visual",
	getInitialState: function(){
		return({
			count: 1,
			tsne_parameters:  _defaults['tsne_parameters'],
			feature_vector_url: _defaults['feature_vector_url'],
			categories : _defaults['categories'],
			categories_checked: _defaults['categories_checked'],
			product_id_to_search: _defaults['product_id_to_search']
		})
	},
	componentDidMount: function(){
		this.resetParameters();
		this.startVisualizationProcessing();
	},
	startVisualizationProcessing: function(){
		this.loadingStarted()
		setTimeout(this.process_visualization,1000)		
	},
	loadingComplete: function(){
		d3.select('#loader')
			.attr("class","hidden")
		d3.select('#myDiv')
			.classed("hidden",false).attr("class","ui fluid container padded segment")

		d3.select(components['vertical-menu'].refs['update_button']).attr("class","ui green inverted fluid button item")
	},
	loadingStarted: function(){
		d3.select('#loader')
			.classed("hidden", false).attr("class","ui active dimmer");

		d3.select('#myDiv')
			.attr("class","hidden")

		d3.select(components['vertical-menu'].refs['update_button']).attr("class","ui inverted fluid button item disabled")
	},
	resetParameters: function(){
		d3.selectAll("svg > *").remove();
	},
	showNoCategorySelected: function(){
		d3.select('#empty_category_message')
			.attr('class','ui red message')

		d3.select('#myDiv')
			.classed('hidden','true')
	},
	resetMessageViews: function(){
		d3.select('#empty_category_message')
			.classed('hidden','true')
	},
	process_visualization: function(){		
		function compare(field){
			return function sort_compare(a,b) {
			  if (a[field] < b[field])
			    return -1;
			  if (a[field] > b[field])
			    return 1;
			  return 0;
			}
		}
		
		var self = this;
		components['visual'] = self;
		
		data = JSON.stringify({
			"categories_checked":self.state['categories_checked']
		})

		if(self.state.feature_vector_url == '/getSimilarProducts'){
			data = JSON.stringify({
				"product":self.state['product_id_to_search']
			})
		}
	
		if(self.state['categories_checked'].length || data['product'] != ""){
			$.ajax({
				url: self.state.feature_vector_url,
				method: 'POST',
				data: data,
				contentType: 'application/json',
				success: function(response){
					var categories = response.results;	
					categories = categories.sort(compare("Id"))
					var ids = []
					var featureVectors = []

					for (i in categories){
						ids.push(categories[i]['Id'])
						featureVectors.push(categories[i]['FeatureVector'])
					}

					data = JSON.stringify({
									"Ids":ids
								})
					$.ajax({
						url: "/getProducts",
						dataType: "json",
						data: data,
						method: 'POST',
						success: function(response){
							// Remove the loading screen
							self.loadingComplete()
							var products = response.results;
							products = products.sort(compare("Id"))
							// TODO: cases when ids are not returned?
							// for(var i in products){
							// 	products.productNames.push(productsRaw[i]['Name'])
							// 	products.productPrices.push(productsRaw[i]['Price'])
							// 	products.imageURLs.push(productsRaw[i]['ImageUrl'])
							// }

							// Start TSNE
							var opt = {}
							opt.epsilon = self.state.tsne_parameters.epsilon; // epsilon is learning rate (10 = default)
							opt.perplexity = self.state.tsne_parameters.perplexity; // roughly how many neighbors each point influences (30 = default)
							opt.dim = self.state.tsne_parameters.dim; // dimensionality of the embedding (2 = default)

							var tsne = new tsnejs.tSNE(opt); // create a tSNE instance

							tsne.initDataRaw(featureVectors);

							var count = self.state.count
							var max_iterations = self.state.tsne_parameters.max_iterations

							// drawPlot here
							var xScale = d3.scaleLinear()
							var yScale = d3.scaleLinear()
							var rScale = d3.scaleLinear()

							tsne.step()
							var Y = tsne.getSolution()
							var dataset = []
							for (index in Y){
								var point = Y[index]
								dataset.push([point[0],point[1]])
							}

							self.resetScales(dataset,xScale,yScale,rScale)

							// Drawing code 
							var svg = d3.select('svg')
							if(svg.empty()){
								svg = d3.select('#myDiv')
										.append('svg')
										.attr('width',w)
										.attr('height',h)
										.attr('style','overflow:overlay')
							}						

							// Scatter plot dots/circle layer
							// var circles = svg.selectAll('circle')
							// 	.data(data.users)
							// 	.enter()
							// 		.append('circle')
							var images = svg.selectAll('image')
								.data(products)
								.enter()
									.append('image')
							// var textLabels = svg.selectAll('text')
							// 	.data(data.users)
							// 	.enter()
							// 		.append('text')
							
							self.applyImageAttrs(images,products,Y,xScale,yScale,rScale)
							// self.applyCircleAttrs(circles,Y,xScale,yScale,rScale)
							// self.applyTextAttrs(textLabels,Y,xScale,yScale,rScale)

							self.setState({
								count: count,
								max_iterations: max_iterations
							})

							intervalID = setInterval(function(){
								if(count <= max_iterations){
									var cost = tsne.step()
									var Y = tsne.getSolution()
									self.plot_data(products,Y,xScale,yScale,rScale)
									self.setState({
										count: count++
									})
								}	
								else{
									clearInterval(intervalID)
								}	
							},0)
						}
					})
				}
			});
		}
		else{
			self.loadingComplete()
			self.showNoCategorySelected()
		}
		
		// d3.json("/static/data/categories.json",function(response){
		// }.bind(this))

		// initialize data. Here we have 3 points and some example pairwise dissimilarities
		// d3.json("/static/data/out2.json",function(data){
		 // }.bind(this))
	},
	plot_data: function(products,Y,xScale,yScale,rScale){
		var dataset = []
		for (index in Y){
			var point = Y[index]
			dataset.push([point[0],point[1]])
		}
		var svg = d3.select('svg')
		var circles = svg.selectAll('circle')
			.data(products)
		var images = svg.selectAll('image')
			.data(products)
		// var textLabels = svg.selectAll('text')
		// 	.data(data.users)

		this.resetScales(dataset,xScale,yScale,rScale)
		this.applyImageAttrs(images,products,Y,xScale,yScale,rScale)
		// this.applyCircleAttrs(circles,Y,xScale,yScale,rScale)
		// this.applyTextAttrs(textLabels,Y,xScale,yScale,rScale)
	},
	resetScales : function(dataset,xScale,yScale,rScale){
		xScale
			.domain([d3.min(dataset, function(d) { return d[0]; }), d3.max(dataset, function(d) { return d[0]; })])
			.range([padding, w - padding]);
		yScale
			.domain([d3.min(dataset, function(d) { return d[1]; }), d3.max(dataset, function(d) { return d[1]; })])
			.range([h-padding,padding]);
		rScale
			.domain([d3.min(dataset, function(d) { return d[1]; }), d3.max(dataset, function(d) { return d[1]; })])
			.range([2, 5]);
	},
	resetAxis : function(xAxis,yAxis,xScale,yScale){
		xAxis = d3.axisBottom(xScale)
		yAxis = d3.axisLeft(yScale)
	},
	redrawAxis : function(svg,xAxis,yAxis){
		svg.select('.x.axis')
			.transition()
		    .call(xAxis);

		svg.select('.y.axis')
			.transition()
		    .call(yAxis);
	},
	applyImageAttrs : function(images,products,Y,xScale,yScale,rScale){
		images
			.transition()
			.attr("xlink:href",function(d,i) {
				return products[i]['ImageUrl'];
			})
			.attr("x",function(d,i) {
				return xScale(Y[i][0]);
			})
			.attr("y",function(d,i) {
				return yScale(Y[i][1]);
			})
			.attr("width", function(d,i) {
				return 50;
			})
			.attr("height", function(d,i) {
				return 50;
			})

		images
			.on("mouseover",function(d,i){				
				var xPosition = parseFloat(d3.select(this).attr("x")) * 1.5 ;
				var yPosition = parseFloat(d3.select(this).attr("y"))/2 + h / 2;

				var svg = d3.select('#tooltip')
				//Update the tooltip position and value

				d3.select(".product_image")
				  .attr("src",d.ImageUrl);

				d3.select("#tooltip")
					.attr("class","ui card")
					.style("left", xPosition + "px")
					.style("top", yPosition + "px")
					.select(".product_name")
				  	.text(d.Name);				

				d3.select("#tooltip")
				  .select(".product_price")
				  .text("$"+d.Price);

				//Show the tooltip
				d3.select("#tooltip").classed("hidden", false);
			})
			.on("mouseout",function(d,i){
				var tooltip = d3.select('#tooltip')
				tooltip.attr("class","hidden")
			})
			.on("click",function(d,i){
				var tooltip = d3.select('#tooltip')
				tooltip.attr("class","hidden")
				console.log(d)
				components.visual.setState({
					count: 1,
					feature_vector_url : "/getSimilarProducts",
					categories_checked: [],
					product_id_to_search: d.Id
				})
				components['vertical-menu'].resetCategories()
				components.visual.resetParameters();
				components.visual.startVisualizationProcessing()	
				components.visual.resetMessageViews()
				width = w = $('.ten.wide.column').width() * 0.9
				d3.select('svg').attr('width',w)	
			})
	},
	applyCircleAttrs : function(circles,Y,xScale,yScale,rScale){
		circles
			.transition()
			.attr("cx",function(d,i) {
				return xScale(Y[i][0]);
			})
			.attr("cy",function(d,i) {
				return yScale(Y[i][1]);
			})
			.attr("r", function(d,i) {
				return rScale(Y[i][1]);
			})

		circles
			.on("mouseover",function(d,i){
				var svg = d3.select('svg')
				svg
					.append('text')
					.attr('x',xScale(Y[i][0]))
					.attr('y',yScale(Y[i][1]))
					.text(d)
					.attr("font-family", "sans-serif")
					.attr("font-size", "3em")
					.attr("fill", "pink");

			})
			.on("mouseout",function(d,i){
				var svg = d3.select('svg')
				svg
					.select('text')
					.remove()

			})
	},
	applyTextAttrs : function(textLabels,Y,xScale,yScale,rScale){
		textLabels
			.transition()
			.attr("x", function(d,i) {
				return xScale(Y[i][0]);
			})
			.attr("y", function(d,i) {
				return yScale(Y[i][1]);
			})
			.text(function(d){
				// return "("+d[0]+","+d[1]+")"
				return d
			})
			.attr("font-family", "sans-serif")
			.attr("font-size", "11px")
			.attr("fill", "red");
	},
	render: function(){
		return(
			React.createElement("div", null, 
				"Number of iterations: ", this.state.count, "/", this.state.max_iterations
			)
		)
	}
})

var Footer = React.createClass({displayName: "Footer",
	componentDidMount: function(){
	},
	render: function(){
		classes = "ui "+ this.props.className
		return( 
		  React.createElement("div", {className: classes, ref: "footerSection"}, 		  	
		  	React.createElement("div", {className: "ui text container center aligned"}, 
		  		React.createElement(DividerBlock, {quantity: "1"}), React.createElement("br", null), 
		  		React.createElement("p", null, "Built with ", React.createElement("a", {href: "https://nodejs.org/en/"}, "NodeJS"), ", ", React.createElement("a", {href: "https://facebook.github.io/react/index.html"}, "ReactJS"), " and ", React.createElement("a", {href: "http://semantic-ui.com/"}, "SemanticUI")), 
		  		React.createElement("div", {className: "ui images"}, 
		  			React.createElement("a", {href: "https://nodejs.org/en/"}, React.createElement("img", {className: "ui centered mini image", src: "/images/nodejs.png"})), 
				  	React.createElement("a", {href: "https://facebook.github.io/react/index.html"}, React.createElement("img", {className: "ui centered mini image", src: "/images/reactjs.svg"})), 
		  			React.createElement("a", {href: "https://semantic-ui.com/"}, React.createElement("img", {className: "ui centered mini circular image", src: "/images/semantic.png"}))
				), 		  		
		  		React.createElement("p", null, "© 2016 Amogh Param", React.createElement("br", null), "Seattle, WA"), 
		  		React.createElement(DividerBlock, {quantity: "2"})
		  	)		  	
		  )
		);
	}
})



ReactDOM.render(
	React.createElement(MenuBar, null),
	document.getElementById('top-menu-container')
)

ReactDOM.render(
	React.createElement(VerticalMenu, null),
	document.getElementById('vertical-menu')
)

ReactDOM.render(
	React.createElement(Visual, null),
	document.getElementById('content')
)

},{"./tsne.js":2}],2:[function(require,module,exports){
// create main global object
var tsnejs = tsnejs || { REVISION: 'ALPHA' };

(function(global) {
  "use strict";

  // utility function
  var assert = function(condition, message) {
    if (!condition) { throw message || "Assertion failed"; }
  }

  // syntax sugar
  var getopt = function(opt, field, defaultval) {
    if(opt.hasOwnProperty(field)) {
      return opt[field];
    } else {
      return defaultval;
    }
  }

  // return 0 mean unit standard deviation random number
  var return_v = false;
  var v_val = 0.0;
  var gaussRandom = function() {
    if(return_v) { 
      return_v = false;
      return v_val; 
    }
    var u = 2*Math.random()-1;
    var v = 2*Math.random()-1;
    var r = u*u + v*v;
    if(r == 0 || r > 1) return gaussRandom();
    var c = Math.sqrt(-2*Math.log(r)/r);
    v_val = v*c; // cache this for next function call for efficiency
    return_v = true;
    return u*c;
  }

  // return random normal number
  var randn = function(mu, std){ return mu+gaussRandom()*std; }

  // utilitity that creates contiguous vector of zeros of size n
  var zeros = function(n) {
    if(typeof(n)==='undefined' || isNaN(n)) { return []; }
    if(typeof ArrayBuffer === 'undefined') {
      // lacking browser support
      var arr = new Array(n);
      for(var i=0;i<n;i++) { arr[i]= 0; }
      return arr;
    } else {
      return new Float64Array(n); // typed arrays are faster
    }
  }

  // utility that returns 2d array filled with random numbers
  // or with value s, if provided
  var randn2d = function(n,d,s) {
    var uses = typeof s !== 'undefined';
    var x = [];
    for(var i=0;i<n;i++) {
      var xhere = [];
      for(var j=0;j<d;j++) { 
        if(uses) {
          xhere.push(s); 
        } else {
          xhere.push(randn(0.0, 1e-4)); 
        }
      }
      x.push(xhere);
    }
    return x;
  }

  // compute L2 distance between two vectors
  var L2 = function(x1, x2) {
    var D = x1.length;
    var d = 0;
    for(var i=0;i<D;i++) { 
      var x1i = x1[i];
      var x2i = x2[i];
      d += (x1i-x2i)*(x1i-x2i);
    }
    return d;
  }

  // compute pairwise distance in all vectors in X
  var xtod = function(X) {
    var N = X.length;
    var dist = zeros(N * N); // allocate contiguous array
    for(var i=0;i<N;i++) {
      for(var j=i+1;j<N;j++) {
        var d = L2(X[i], X[j]);
        dist[i*N+j] = d;
        dist[j*N+i] = d;
      }
    }
    return dist;
  }

  // compute (p_{i|j} + p_{j|i})/(2n)
  var d2p = function(D, perplexity, tol) {
    var Nf = Math.sqrt(D.length); // this better be an integer
    var N = Math.floor(Nf);
    assert(N === Nf, "D should have square number of elements.");
    var Htarget = Math.log(perplexity); // target entropy of distribution
    var P = zeros(N * N); // temporary probability matrix

    var prow = zeros(N); // a temporary storage compartment
    for(var i=0;i<N;i++) {
      var betamin = -Infinity;
      var betamax = Infinity;
      var beta = 1; // initial value of precision
      var done = false;
      var maxtries = 50;

      // perform binary search to find a suitable precision beta
      // so that the entropy of the distribution is appropriate
      var num = 0;
      while(!done) {
        //debugger;

        // compute entropy and kernel row with beta precision
        var psum = 0.0;
        for(var j=0;j<N;j++) {
          var pj = Math.exp(- D[i*N+j] * beta);
          if(i===j) { pj = 0; } // we dont care about diagonals
          prow[j] = pj;
          psum += pj;
        }
        // normalize p and compute entropy
        var Hhere = 0.0;
        for(var j=0;j<N;j++) {
          var pj = prow[j] / psum;
          prow[j] = pj;
          if(pj > 1e-7) Hhere -= pj * Math.log(pj);
        }

        // adjust beta based on result
        if(Hhere > Htarget) {
          // entropy was too high (distribution too diffuse)
          // so we need to increase the precision for more peaky distribution
          betamin = beta; // move up the bounds
          if(betamax === Infinity) { beta = beta * 2; }
          else { beta = (beta + betamax) / 2; }

        } else {
          // converse case. make distrubtion less peaky
          betamax = beta;
          if(betamin === -Infinity) { beta = beta / 2; }
          else { beta = (beta + betamin) / 2; }
        }

        // stopping conditions: too many tries or got a good precision
        num++;
        if(Math.abs(Hhere - Htarget) < tol) { done = true; }
        if(num >= maxtries) { done = true; }
      }

      // console.log('data point ' + i + ' gets precision ' + beta + ' after ' + num + ' binary search steps.');
      // copy over the final prow to P at row i
      for(var j=0;j<N;j++) { P[i*N+j] = prow[j]; }

    } // end loop over examples i

    // symmetrize P and normalize it to sum to 1 over all ij
    var Pout = zeros(N * N);
    var N2 = N*2;
    for(var i=0;i<N;i++) {
      for(var j=0;j<N;j++) {
        Pout[i*N+j] = Math.max((P[i*N+j] + P[j*N+i])/N2, 1e-100);
      }
    }

    return Pout;
  }

  // helper function
  function sign(x) { return x > 0 ? 1 : x < 0 ? -1 : 0; }

  var tSNE = function(opt) {
    var opt = opt || {};
    this.perplexity = getopt(opt, "perplexity", 30); // effective number of nearest neighbors
    this.dim = getopt(opt, "dim", 2); // by default 2-D tSNE
    this.epsilon = getopt(opt, "epsilon", 10); // learning rate

    this.iter = 0;
  }

  tSNE.prototype = {

    // this function takes a set of high-dimensional points
    // and creates matrix P from them using gaussian kernel
    initDataRaw: function(X) {
      var N = X.length;
      var D = X[0].length;
      assert(N > 0, " X is empty? You must have some data!");
      assert(D > 0, " X[0] is empty? Where is the data?");
      var dists = xtod(X); // convert X to distances using gaussian kernel
      this.P = d2p(dists, this.perplexity, 1e-4); // attach to object
      this.N = N; // back up the size of the dataset
      this.initSolution(); // refresh this
    },

    // this function takes a given distance matrix and creates
    // matrix P from them.
    // D is assumed to be provided as a list of lists, and should be symmetric
    initDataDist: function(D) {
      var N = D.length;
      assert(N > 0, " X is empty? You must have some data!");
      // convert D to a (fast) typed array version
      var dists = zeros(N * N); // allocate contiguous array
      for(var i=0;i<N;i++) {
        for(var j=i+1;j<N;j++) {
          var d = D[i][j];
          dists[i*N+j] = d;
          dists[j*N+i] = d;
        }
      }
      this.P = d2p(dists, this.perplexity, 1e-4);
      this.N = N;
      this.initSolution(); // refresh this
    },

    // (re)initializes the solution to random
    initSolution: function() {
      // generate random solution to t-SNE
      this.Y = randn2d(this.N, this.dim); // the solution
      this.gains = randn2d(this.N, this.dim, 1.0); // step gains to accelerate progress in unchanging directions
      this.ystep = randn2d(this.N, this.dim, 0.0); // momentum accumulator
      this.iter = 0;
    },

    // return pointer to current solution
    getSolution: function() {
      return this.Y;
    },

    // perform a single step of optimization to improve the embedding
    step: function() {
      this.iter += 1;
      var N = this.N;

      var cg = this.costGrad(this.Y); // evaluate gradient
      var cost = cg.cost;
      var grad = cg.grad;

      // perform gradient step
      var ymean = zeros(this.dim);
      for(var i=0;i<N;i++) {
        for(var d=0;d<this.dim;d++) {
          var gid = grad[i][d];
          var sid = this.ystep[i][d];
          var gainid = this.gains[i][d];

          // compute gain update
          var newgain = sign(gid) === sign(sid) ? gainid * 0.8 : gainid + 0.2;
          if(newgain < 0.01) newgain = 0.01; // clamp
          this.gains[i][d] = newgain; // store for next turn

          // compute momentum step direction
          var momval = this.iter < 250 ? 0.5 : 0.8;
          var newsid = momval * sid - this.epsilon * newgain * grad[i][d];
          this.ystep[i][d] = newsid; // remember the step we took

          // step!
          this.Y[i][d] += newsid; 

          ymean[d] += this.Y[i][d]; // accumulate mean so that we can center later
        }
      }

      // reproject Y to be zero mean
      for(var i=0;i<N;i++) {
        for(var d=0;d<this.dim;d++) {
          this.Y[i][d] -= ymean[d]/N;
        }
      }

      //if(this.iter%100===0) console.log('iter ' + this.iter + ', cost: ' + cost);
      return cost; // return current cost
    },

    // for debugging: gradient check
    debugGrad: function() {
      var N = this.N;

      var cg = this.costGrad(this.Y); // evaluate gradient
      var cost = cg.cost;
      var grad = cg.grad;

      var e = 1e-5;
      for(var i=0;i<N;i++) {
        for(var d=0;d<this.dim;d++) {
          var yold = this.Y[i][d];

          this.Y[i][d] = yold + e;
          var cg0 = this.costGrad(this.Y);

          this.Y[i][d] = yold - e;
          var cg1 = this.costGrad(this.Y);
          
          var analytic = grad[i][d];
          var numerical = (cg0.cost - cg1.cost) / ( 2 * e );
          console.log(i + ',' + d + ': gradcheck analytic: ' + analytic + ' vs. numerical: ' + numerical);

          this.Y[i][d] = yold;
        }
      }
    },

    // return cost and gradient, given an arrangement
    costGrad: function(Y) {
      var N = this.N;
      var dim = this.dim; // dim of output space
      var P = this.P;

      var pmul = this.iter < 100 ? 4 : 1; // trick that helps with local optima

      // compute current Q distribution, unnormalized first
      var Qu = zeros(N * N);
      var qsum = 0.0;
      for(var i=0;i<N;i++) {
        for(var j=i+1;j<N;j++) {
          var dsum = 0.0;
          for(var d=0;d<dim;d++) {
            var dhere = Y[i][d] - Y[j][d];
            dsum += dhere * dhere;
          }
          var qu = 1.0 / (1.0 + dsum); // Student t-distribution
          Qu[i*N+j] = qu;
          Qu[j*N+i] = qu;
          qsum += 2 * qu;
        }
      }
      // normalize Q distribution to sum to 1
      var NN = N*N;
      var Q = zeros(NN);
      for(var q=0;q<NN;q++) { Q[q] = Math.max(Qu[q] / qsum, 1e-100); }

      var cost = 0.0;
      var grad = [];
      for(var i=0;i<N;i++) {
        var gsum = new Array(dim); // init grad for point i
        for(var d=0;d<dim;d++) { gsum[d] = 0.0; }
        for(var j=0;j<N;j++) {
          cost += - P[i*N+j] * Math.log(Q[i*N+j]); // accumulate cost (the non-constant portion at least...)
          var premult = 4 * (pmul * P[i*N+j] - Q[i*N+j]) * Qu[i*N+j];
          for(var d=0;d<dim;d++) {
            gsum[d] += premult * (Y[i][d] - Y[j][d]);
          }
        }
        grad.push(gsum);
      }

      return {cost: cost, grad: grad};
    }
  }

  global.tSNE = tSNE; // export tSNE class
})(tsnejs);


// export the library to window, or to module in nodejs
(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.tsnejs = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(tsnejs);

},{}]},{},[1]);
