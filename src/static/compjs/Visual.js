(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var tsnejs = require('./tsne.js')
var MenuBar = React.createClass({displayName: "MenuBar",
	render: function(){
		return(
			React.createElement("div", {className: "ui top blue inverted fixed menu"}, 
			  React.createElement("a", {className: "item", href: "/"}, 
			    "Vusearch"
			  )
			)
		)
	}
})

var Visual = React.createClass({displayName: "Visual",
	getInitialState: function(){
		return({
			count: 1,
			max_iterations: 500
		})
	},
	componentDidMount: function(){
		setTimeout(this.process_visualization,1000)
	},
	process_visualization: function(){
		var opt = {}
		opt.epsilon = 10; // epsilon is learning rate (10 = default)
		opt.perplexity = 30; // roughly how many neighbors each point influences (30 = default)
		opt.dim = 2; // dimensionality of the embedding (2 = default)

		var tsne = new tsnejs.tSNE(opt); // create a tSNE instance

		// initialize data. Here we have 3 points and some example pairwise dissimilarities
		var dists = [[42000,5850,3,1,2],[38500,4000,2,1,1],[49500,3060,3,1,1],[60500,6650,3,1,2],[61000,6360,2,1,1],[66000,4160,3,1,1],[66000,3880,3,2,2],[69000,4160,3,1,3],[83800,4800,3,1,1],[88500,5500,3,2,4],[90000,7200,3,2,1],[30500,3000,2,1,1],[27000,1700,3,1,2],[36000,2880,3,1,1],[37000,3600,2,1,1],[37900,3185,2,1,1],[40500,3300,3,1,2],[40750,5200,4,1,3],[45000,3450,1,1,1],[45000,3986,2,2,1],[48500,4785,3,1,2],[65900,4510,4,2,2],[37900,4000,3,1,2],[38000,3934,2,1,1],[42000,4960,2,1,1],[42300,3000,2,1,2],[43500,3800,2,1,1],[44000,4960,2,1,1],[44500,3000,3,1,1],[44900,4500,3,1,2],[45000,3500,2,1,1],[48000,3500,4,1,2],[49000,4000,2,1,1],[51500,4500,2,1,1],[61000,6360,2,1,2],[61000,4500,2,1,1],[61700,4032,2,1,1],[67000,5170,3,1,4],[82000,5400,4,2,2],[54500,3150,2,2,1],[66500,3745,3,1,2],[70000,4520,3,1,2],[82000,4640,4,1,2],[92000,8580,5,3,2],[38000,2000,2,1,2],[44000,2160,3,1,2],[41000,3040,2,1,1],[43000,3090,3,1,2],[48000,4960,4,1,3],[54800,3350,3,1,2],[55000,5300,5,2,2],[57000,4100,4,1,1],[68000,9166,2,1,1],[95000,4040,3,1,2],[38000,3630,3,3,2],[25000,3620,2,1,1],[25245,2400,3,1,1],[56000,7260,3,2,1],[35500,4400,3,1,2],[30000,2400,3,1,2],[48000,4120,2,1,2],[48000,4750,2,1,1],[52000,4280,2,1,1],[54000,4820,3,1,2],[56000,5500,4,1,2],[60000,5500,3,1,2],[60000,5040,3,1,2],[67000,6000,2,1,1],[47000,2500,2,1,1],[70000,4095,3,1,2],[45000,4095,2,1,1],[51000,3150,3,1,2],[32500,1836,2,1,1],[34000,2475,3,1,2],[35000,3210,3,1,2],[36000,3180,3,1,1],[45000,1650,3,1,2],[47000,3180,4,1,2],[55000,3180,2,2,1],[63900,6360,2,1,1],[50000,4240,3,1,2],[35000,3240,2,1,1],[50000,3650,3,1,2],[43000,3240,3,1,2],[55500,3780,2,1,2],[57000,6480,3,1,2],[60000,5850,2,1,1],[78000,3150,3,2,1],[35000,3000,2,1,1],[44000,3090,2,1,1],[47000,6060,3,1,1],[58000,5900,4,2,2],[163000,7420,4,1,2],[128000,8500,3,2,4],[123500,8050,3,1,1],[39000,6800,2,1,1],[53900,8250,3,1,1],[59900,8250,3,1,1],[35000,3500,2,1,1],[43000,2835,2,1,1],[57000,4500,3,2,2],[79000,3300,3,3,2],[125000,4320,3,1,2],[132000,3500,4,2,2],[58000,4992,3,2,2],[43000,4600,2,1,1],[48000,3720,2,1,1],[58500,3680,3,2,2],[73000,3000,3,2,2],[63500,3750,2,1,1],[43000,5076,3,1,1],[46500,4500,2,1,1],[92000,5000,3,1,2],[75000,4260,4,1,2],[75000,6540,4,2,2],[85000,3700,4,1,2],[93000,3760,3,1,2],[94500,4000,3,2,2],[106500,4300,3,2,2],[116000,6840,5,1,2],[61500,4400,2,1,1],[80000,10500,4,2,2],[37000,4400,2,1,1],[59500,4840,3,1,2],[70000,4120,2,1,1],[95000,4260,4,2,2],[117000,5960,3,3,2],[122500,8800,3,2,2],[123500,4560,3,2,2],[127000,4600,3,2,2],[35000,4840,2,1,2],[44500,3850,3,1,2],[49900,4900,3,1,2],[50500,3850,3,1,1],[65000,3760,3,1,1],[90000,6000,4,2,4],[46000,4370,3,1,2],[35000,7700,2,1,1],[26500,2990,2,1,1],[43000,3750,3,1,2],[56000,3000,3,1,2],[40000,2650,3,1,2],[51000,4500,4,2,2],[51000,4500,2,1,1],[57250,4500,3,1,2],[44000,4500,2,1,2],[61000,2175,3,1,2],[62000,4500,3,2,3],[80000,4800,5,2,3],[50000,4600,4,1,2],[59900,3450,3,1,2],[35500,3000,3,1,2],[37000,3600,2,2,2],[42000,3600,3,1,2],[48000,3750,3,1,1],[60000,2610,4,3,2],[60000,2953,3,1,2],[60000,2747,4,2,2],[62000,1905,5,1,2],[63000,3968,3,1,2],[63900,3162,3,1,2],[130000,6000,4,1,2],[25000,2910,3,1,1],[50000,2135,3,2,2],[52900,3120,3,1,2],[62000,4075,3,1,1],[73500,3410,3,1,2],[38000,2800,3,1,1],[46000,2684,2,1,1],[48000,3100,3,1,2],[52500,3630,2,1,1],[32000,1950,3,1,1],[38000,2430,3,1,1],[46000,4320,3,1,1],[50000,3036,3,1,2],[57500,3630,3,2,2],[70000,5400,4,1,2],[69900,3420,4,2,2],[74500,3180,3,2,2],[42000,3660,4,1,2],[60000,4410,2,1,1],[50000,3990,3,1,2],[58000,4340,3,1,1],[63900,3510,3,1,2],[28000,3420,5,1,2],[54000,3420,2,1,2],[44700,5495,3,1,1],[47000,3480,4,1,2],[50000,7424,3,1,1],[57250,3460,4,1,2],[67000,3630,3,1,2],[52500,3630,2,1,1],[42000,3480,3,1,2],[57500,3460,3,2,1],[33000,3180,2,1,1],[34400,3635,2,1,1],[40000,3960,3,1,1],[40500,4350,3,1,2],[46500,3930,2,1,1],[52000,3570,3,1,2],[53000,3600,3,1,1],[53900,2520,5,2,1],[50000,3480,3,1,1],[55500,3180,4,2,2],[56000,3290,2,1,1],[60000,4000,4,2,2],[60000,2325,3,1,2],[69500,4350,2,1,1],[72000,3540,2,1,1],[92500,3960,3,1,1],[40500,2640,2,1,1],[42000,2700,2,1,1],[47900,2700,3,1,1],[52000,3180,3,1,2],[62000,3500,4,1,2],[41000,3630,2,1,1],[138300,6000,4,3,2],[42000,3150,3,1,2],[47000,3792,4,1,2],[64500,3510,3,1,3],[46000,3120,3,1,2],[58000,3000,4,1,3],[70100,4200,3,1,2],[78500,2817,4,2,2],[87250,3240,4,1,3],[70800,2800,3,2,2],[56000,3816,2,1,1],[48000,3185,2,1,1],[68000,6321,3,1,2],[79000,3650,3,2,2],[80000,4700,4,1,2],[87000,6615,4,2,2],[25000,3850,3,1,2],[32500,3970,1,1,1],[36000,3000,2,1,2],[42500,4352,4,1,2],[43000,3630,4,1,2],[50000,3600,6,1,2],[26000,3000,2,1,1],[30000,3000,4,1,2],[34000,2787,4,2,2],[52000,3000,2,1,2],[70000,4770,3,1,1],[27000,3649,2,1,1],[32500,3970,3,1,2],[37200,2910,2,1,1],[38000,3480,2,1,1],[42000,6615,3,1,2],[44500,3500,2,1,1],[45000,3450,3,1,2],[48500,3450,3,1,1],[52000,3520,2,2,1],[53900,6930,4,1,2],[60000,4600,3,2,2],[61000,4360,4,1,2],[64500,3450,3,1,2],[71000,4410,4,3,2],[75500,4600,2,2,1],[33500,3640,2,1,1],[41000,6000,2,1,1],[41000,5400,4,1,2],[46200,3640,4,1,2],[48500,3640,2,1,1],[48900,4040,2,1,1],[50000,3640,2,1,1],[51000,3640,2,1,1],[52500,5640,2,1,1],[52500,3600,2,1,1],[54000,3600,2,1,1],[59000,4632,4,1,2],[60000,3640,3,2,2],[63000,4900,2,1,2],[64000,4510,4,1,2],[64900,4100,2,2,1],[65000,3640,3,1,2],[66000,5680,3,1,2],[70000,6300,3,1,1],[65500,4000,3,1,2],[57000,3960,3,1,2],[52000,5960,3,1,2],[54000,5830,2,1,1],[74500,4500,4,2,1],[90000,4100,3,2,3],[45000,6750,2,1,1],[45000,9000,3,1,2],[65000,2550,3,1,2],[55000,7152,3,1,2],[62000,6450,4,1,2],[30000,3360,2,1,1],[34000,3264,2,1,1],[38000,4000,3,1,1],[39000,4000,3,1,2],[45000,3069,2,1,1],[47000,4040,2,1,1],[47500,4040,2,1,1],[49000,3185,2,1,1],[50000,5900,2,1,1],[50000,3120,3,1,2],[52900,5450,2,1,1],[53000,4040,2,1,1],[55000,4080,2,1,1],[56000,8080,3,1,1],[58500,4040,2,1,2],[59500,4080,3,1,2],[60000,5800,3,1,1],[64000,5885,2,1,1],[67000,9667,4,2,2],[68100,3420,4,2,2],[70000,5800,2,1,1],[72000,7600,4,1,2],[57500,5400,3,1,1],[69900,4995,4,2,1],[70000,3000,3,1,2],[75000,5500,3,2,1],[76900,6450,3,2,1],[78000,6210,4,1,4],[80000,5000,3,1,4],[82000,5000,3,1,3],[83000,5828,4,1,4],[83000,5200,3,1,3],[83900,5500,3,1,3],[88500,6350,3,2,3],[93000,8250,3,2,3],[98000,6000,3,1,1],[98500,7700,3,2,1],[99000,8880,3,2,2],[101000,8880,2,1,1],[110000,6480,3,2,4],[115442,7000,3,2,4],[120000,8875,3,1,1],[124000,7155,3,2,1],[175000,8960,4,4,4],[50000,7350,2,1,1],[55000,3850,2,1,1],[60000,7000,3,1,1],[61000,7770,2,1,1],[106000,7440,3,2,1],[155000,7500,3,3,1],[141000,8100,4,1,2],[62500,3900,3,1,2],[70000,2970,3,1,3],[73000,3000,3,1,2],[80000,10500,2,1,1],[80000,5500,3,2,2],[88000,4500,3,1,4],[49000,3850,3,1,1],[52000,4130,3,2,2],[59500,4046,3,1,2],[60000,4079,3,1,3],[64000,4000,3,1,2],[64500,9860,3,1,1],[68500,7000,3,1,2],[78500,7980,3,1,1],[86000,6800,2,1,1],[86900,4300,6,2,2],[75000,10269,3,1,1],[78000,6100,3,1,3],[95000,6420,3,2,3],[97000,12090,4,2,2],[107000,6600,3,1,4],[130000,6600,4,2,2],[145000,8580,4,3,4],[175000,9960,3,2,2],[72000,10700,3,1,2],[84900,15600,3,1,1],[99000,13200,2,1,1],[114000,9000,4,2,4],[120000,7950,5,2,2],[145000,16200,5,3,2],[79000,6100,3,2,1],[82000,6360,3,1,1],[85000,6420,3,1,1],[100500,6360,4,2,3],[122000,6540,4,2,2],[126500,6420,3,2,2],[133000,6550,4,2,2],[140000,5750,3,2,4],[190000,7420,4,2,3],[84000,7160,3,1,1],[97000,4000,3,2,2],[103500,9000,4,2,4],[112500,6550,3,1,2],[140000,13200,3,1,2],[74700,7085,3,1,1],[78000,6600,4,2,2],[78900,6900,3,1,1],[83900,11460,3,1,3],[85000,7020,3,1,1],[85000,6540,3,1,1],[86000,8000,3,1,1],[86900,9620,3,1,1],[94500,10500,3,2,1],[96000,5020,3,1,4],[106000,7440,3,2,4],[72000,6600,3,1,1],[74500,7200,3,1,2],[77000,6710,3,2,2],[80750,6660,4,2,2],[82900,7000,3,1,1],[85000,7231,3,1,2],[92500,7410,3,1,1],[76000,7800,3,1,1],[77500,6825,3,1,1],[80000,6360,3,1,3],[80000,6600,4,2,1],[86000,6900,3,2,1],[87000,6600,3,1,1],[87500,6420,3,1,3],[89000,6600,3,2,1],[89900,6600,3,2,3],[90000,9000,3,1,1],[95000,6500,3,2,3],[112000,6360,3,2,4],[31900,5300,3,1,1],[52000,2850,3,2,2],[90000,6400,3,1,1],[1e+05,11175,3,1,1],[91700,6750,2,1,1],[174500,7500,4,2,2],[94700,6000,3,1,2],[68000,10240,2,1,1],[80000,5136,3,1,2],[61100,3400,3,1,2],[62900,2880,3,1,2],[65500,3840,3,1,2],[66000,2870,2,1,2],[49500,5320,2,1,1],[50000,3512,2,1,1],[53500,3480,2,1,1],[58550,3600,3,1,1],[64500,3520,2,1,2],[65000,5320,3,1,2],[69000,6040,3,1,1],[73000,11410,2,1,2],[75000,8400,3,1,2],[75000,5300,4,2,1],[132000,7800,3,2,2],[60000,3520,3,1,2],[65000,5360,3,1,2],[69000,6862,3,1,2],[51900,3520,3,1,1],[57000,4050,2,1,2],[65000,3520,3,1,1],[79500,4400,4,1,2],[72500,5720,2,1,2],[104900,11440,4,1,2],[114900,7482,3,2,3],[120000,5500,4,2,2],[58000,4320,3,1,2],[67000,5400,2,1,2],[67000,4320,3,1,1],[69000,4815,2,1,1],[73000,6100,3,1,1],[73500,7980,3,1,1],[74900,6050,3,1,1],[75000,3800,3,1,2],[79500,5400,5,1,2],[120900,6000,3,2,4],[44555,2398,3,1,1],[47000,2145,3,1,2],[47600,2145,3,1,2],[49000,2145,3,1,3],[49000,2610,3,1,2],[49000,1950,3,2,2],[49500,2145,3,1,3],[52000,2275,3,1,3],[54000,2856,3,1,3],[55000,2015,3,1,2],[55000,2176,2,1,2],[56000,2145,4,2,1],[60000,2145,3,1,3],[60500,2787,3,1,1],[50000,9500,3,1,2],[64900,4990,4,2,2],[93000,6670,3,1,3],[85000,6254,4,2,1],[61500,10360,2,1,1],[88500,5500,3,2,1],[88000,5450,4,2,1],[89000,5500,3,1,3],[89500,6000,4,1,3],[95000,5700,3,1,1],[95500,6600,2,2,4],[51500,4000,2,1,1],[62900,4880,3,1,1],[118500,4880,4,2,2],[42900,8050,2,1,1],[44100,8100,2,1,1],[47000,5880,3,1,1],[50000,5880,2,1,1],[50000,12944,3,1,1],[53000,6020,3,1,1],[53000,4050,2,1,1],[54000,8400,2,1,1],[58500,5600,2,1,1],[59000,5985,3,1,1],[60000,4500,3,1,1],[62900,4920,3,1,2],[64000,8250,3,1,1],[65000,8400,4,1,4],[67900,6440,2,1,1],[68500,8100,4,1,4],[70000,6720,3,1,1],[70500,5948,3,1,2],[71500,8150,3,2,1],[71900,4800,2,1,1],[75000,9800,4,2,2],[75000,8520,3,1,1],[87000,8372,3,1,3],[64000,4040,3,1,2],[70000,4646,3,1,2],[47500,4775,4,1,2],[62600,4950,4,1,2],[66000,5010,3,1,2],[58900,6060,2,1,1],[53000,3584,2,1,1],[95000,6000,3,2,3],[96500,6000,4,2,4],[101000,6240,4,2,2],[102000,6000,3,2,2],[103000,7680,4,2,4],[105000,6000,4,2,4],[108000,6000,4,2,4],[110000,6000,4,2,4],[113000,6000,4,2,4],[120000,7475,3,2,4],[105000,5150,3,2,4],[106000,6325,3,1,4],[107500,6000,3,2,4],[108000,6000,3,2,3],[113750,6000,3,1,4],[120000,7000,3,1,4],[70000,12900,3,1,1],[71000,7686,3,1,1],[82000,5000,3,1,3],[82000,5800,3,2,4],[82500,6000,3,2,4],[83000,4800,3,1,3],[84000,6500,3,2,3],[85000,7320,4,2,2],[85000,6525,3,2,4],[91500,4800,3,2,4],[94000,6000,3,2,4],[103000,6000,3,2,4],[105000,6000,3,2,2],[105000,6000,3,1,2]];
		
		tsne.initDataRaw(dists);
		
		var count = this.state.count
		var max_iterations = this.state.max_iterations

		var layout = {
		  autosize: false,
		  width: 1000,
		  height: 500,
		  xaxis:{
		  	showticklabels:false,
		  	zeroline: false,
		  	showgrid: false
		  },
		  yaxis:{
		  	showticklabels: false,
		  	zeroline: false,
		  	showgrid: false
		  }
		};

		$('#myDiv').css("pointer-events","none")
		Plotly.newPlot('myDiv',[],layout,{staticPlot: true});
		this.setState({
			count: count,
			max_iterations: max_iterations
		})
		intervalID = setInterval(function(){
			if(count < max_iterations){
				tsne.step()
				this.plot_data(tsne.getSolution())
				// count++
				this.setState({
					count: count++
				})
			}	
			else{
				clearInterval(intervalID)
			}	
		}.bind(this),0.01)
	},
	plot_data: function(Y){
		var x = []
		var y = []
		
		for (index in Y){
			var point = Y[index]
			x.push(point[0])
			y.push(point[1])
		}

		var trace1 = {
		  type: 'scatter',
		  x: x,
		  y: y,
		  mode: 'markers',
		  marker: {
		    // color: 'rgba(156, 165, 196, 0.95)',
		    // line: {
		    //   color: 'rgba(156, 165, 196, 1.0)',
		    //   width: 1,
		    // },
		    symbol: 'circle',
		    size: 3
		  }
		};

		var data = [trace1];

		myDiv = document.getElementById('myDiv')
		myDiv.data = data
		Plotly.redraw(myDiv);
	},
	render: function(){		
		return(
			React.createElement("div", null, 				
				React.createElement(MenuBar, null), 
				React.createElement("div", {className: "ui text container filter"}				
				), 
				React.createElement("div", {className: "ui container"}, 
					"Number of iterations: ", this.state.count, "/", this.state.max_iterations
				)
			)
		)
	}
})

ReactDOM.render(
	React.createElement(Visual, null),
	document.getElementById('content')
)

},{"./tsne.js":2}],2:[function(require,module,exports){
// create main global object
var tsnejs = tsnejs || { REVISION: 'ALPHA' };

(function(global) {
  "use strict";

  // utility function
  var assert = function(condition, message) {
    if (!condition) { throw message || "Assertion failed"; }
  }

  // syntax sugar
  var getopt = function(opt, field, defaultval) {
    if(opt.hasOwnProperty(field)) {
      return opt[field];
    } else {
      return defaultval;
    }
  }

  // return 0 mean unit standard deviation random number
  var return_v = false;
  var v_val = 0.0;
  var gaussRandom = function() {
    if(return_v) { 
      return_v = false;
      return v_val; 
    }
    var u = 2*Math.random()-1;
    var v = 2*Math.random()-1;
    var r = u*u + v*v;
    if(r == 0 || r > 1) return gaussRandom();
    var c = Math.sqrt(-2*Math.log(r)/r);
    v_val = v*c; // cache this for next function call for efficiency
    return_v = true;
    return u*c;
  }

  // return random normal number
  var randn = function(mu, std){ return mu+gaussRandom()*std; }

  // utilitity that creates contiguous vector of zeros of size n
  var zeros = function(n) {
    if(typeof(n)==='undefined' || isNaN(n)) { return []; }
    if(typeof ArrayBuffer === 'undefined') {
      // lacking browser support
      var arr = new Array(n);
      for(var i=0;i<n;i++) { arr[i]= 0; }
      return arr;
    } else {
      return new Float64Array(n); // typed arrays are faster
    }
  }

  // utility that returns 2d array filled with random numbers
  // or with value s, if provided
  var randn2d = function(n,d,s) {
    var uses = typeof s !== 'undefined';
    var x = [];
    for(var i=0;i<n;i++) {
      var xhere = [];
      for(var j=0;j<d;j++) { 
        if(uses) {
          xhere.push(s); 
        } else {
          xhere.push(randn(0.0, 1e-4)); 
        }
      }
      x.push(xhere);
    }
    return x;
  }

  // compute L2 distance between two vectors
  var L2 = function(x1, x2) {
    var D = x1.length;
    var d = 0;
    for(var i=0;i<D;i++) { 
      var x1i = x1[i];
      var x2i = x2[i];
      d += (x1i-x2i)*(x1i-x2i);
    }
    return d;
  }

  // compute pairwise distance in all vectors in X
  var xtod = function(X) {
    var N = X.length;
    var dist = zeros(N * N); // allocate contiguous array
    for(var i=0;i<N;i++) {
      for(var j=i+1;j<N;j++) {
        var d = L2(X[i], X[j]);
        dist[i*N+j] = d;
        dist[j*N+i] = d;
      }
    }
    return dist;
  }

  // compute (p_{i|j} + p_{j|i})/(2n)
  var d2p = function(D, perplexity, tol) {
    var Nf = Math.sqrt(D.length); // this better be an integer
    var N = Math.floor(Nf);
    assert(N === Nf, "D should have square number of elements.");
    var Htarget = Math.log(perplexity); // target entropy of distribution
    var P = zeros(N * N); // temporary probability matrix

    var prow = zeros(N); // a temporary storage compartment
    for(var i=0;i<N;i++) {
      var betamin = -Infinity;
      var betamax = Infinity;
      var beta = 1; // initial value of precision
      var done = false;
      var maxtries = 50;

      // perform binary search to find a suitable precision beta
      // so that the entropy of the distribution is appropriate
      var num = 0;
      while(!done) {
        //debugger;

        // compute entropy and kernel row with beta precision
        var psum = 0.0;
        for(var j=0;j<N;j++) {
          var pj = Math.exp(- D[i*N+j] * beta);
          if(i===j) { pj = 0; } // we dont care about diagonals
          prow[j] = pj;
          psum += pj;
        }
        // normalize p and compute entropy
        var Hhere = 0.0;
        for(var j=0;j<N;j++) {
          var pj = prow[j] / psum;
          prow[j] = pj;
          if(pj > 1e-7) Hhere -= pj * Math.log(pj);
        }

        // adjust beta based on result
        if(Hhere > Htarget) {
          // entropy was too high (distribution too diffuse)
          // so we need to increase the precision for more peaky distribution
          betamin = beta; // move up the bounds
          if(betamax === Infinity) { beta = beta * 2; }
          else { beta = (beta + betamax) / 2; }

        } else {
          // converse case. make distrubtion less peaky
          betamax = beta;
          if(betamin === -Infinity) { beta = beta / 2; }
          else { beta = (beta + betamin) / 2; }
        }

        // stopping conditions: too many tries or got a good precision
        num++;
        if(Math.abs(Hhere - Htarget) < tol) { done = true; }
        if(num >= maxtries) { done = true; }
      }

      // console.log('data point ' + i + ' gets precision ' + beta + ' after ' + num + ' binary search steps.');
      // copy over the final prow to P at row i
      for(var j=0;j<N;j++) { P[i*N+j] = prow[j]; }

    } // end loop over examples i

    // symmetrize P and normalize it to sum to 1 over all ij
    var Pout = zeros(N * N);
    var N2 = N*2;
    for(var i=0;i<N;i++) {
      for(var j=0;j<N;j++) {
        Pout[i*N+j] = Math.max((P[i*N+j] + P[j*N+i])/N2, 1e-100);
      }
    }

    return Pout;
  }

  // helper function
  function sign(x) { return x > 0 ? 1 : x < 0 ? -1 : 0; }

  var tSNE = function(opt) {
    var opt = opt || {};
    this.perplexity = getopt(opt, "perplexity", 30); // effective number of nearest neighbors
    this.dim = getopt(opt, "dim", 2); // by default 2-D tSNE
    this.epsilon = getopt(opt, "epsilon", 10); // learning rate

    this.iter = 0;
  }

  tSNE.prototype = {

    // this function takes a set of high-dimensional points
    // and creates matrix P from them using gaussian kernel
    initDataRaw: function(X) {
      var N = X.length;
      var D = X[0].length;
      assert(N > 0, " X is empty? You must have some data!");
      assert(D > 0, " X[0] is empty? Where is the data?");
      var dists = xtod(X); // convert X to distances using gaussian kernel
      this.P = d2p(dists, this.perplexity, 1e-4); // attach to object
      this.N = N; // back up the size of the dataset
      this.initSolution(); // refresh this
    },

    // this function takes a given distance matrix and creates
    // matrix P from them.
    // D is assumed to be provided as a list of lists, and should be symmetric
    initDataDist: function(D) {
      var N = D.length;
      assert(N > 0, " X is empty? You must have some data!");
      // convert D to a (fast) typed array version
      var dists = zeros(N * N); // allocate contiguous array
      for(var i=0;i<N;i++) {
        for(var j=i+1;j<N;j++) {
          var d = D[i][j];
          dists[i*N+j] = d;
          dists[j*N+i] = d;
        }
      }
      this.P = d2p(dists, this.perplexity, 1e-4);
      this.N = N;
      this.initSolution(); // refresh this
    },

    // (re)initializes the solution to random
    initSolution: function() {
      // generate random solution to t-SNE
      this.Y = randn2d(this.N, this.dim); // the solution
      this.gains = randn2d(this.N, this.dim, 1.0); // step gains to accelerate progress in unchanging directions
      this.ystep = randn2d(this.N, this.dim, 0.0); // momentum accumulator
      this.iter = 0;
    },

    // return pointer to current solution
    getSolution: function() {
      return this.Y;
    },

    // perform a single step of optimization to improve the embedding
    step: function() {
      this.iter += 1;
      var N = this.N;

      var cg = this.costGrad(this.Y); // evaluate gradient
      var cost = cg.cost;
      var grad = cg.grad;

      // perform gradient step
      var ymean = zeros(this.dim);
      for(var i=0;i<N;i++) {
        for(var d=0;d<this.dim;d++) {
          var gid = grad[i][d];
          var sid = this.ystep[i][d];
          var gainid = this.gains[i][d];

          // compute gain update
          var newgain = sign(gid) === sign(sid) ? gainid * 0.8 : gainid + 0.2;
          if(newgain < 0.01) newgain = 0.01; // clamp
          this.gains[i][d] = newgain; // store for next turn

          // compute momentum step direction
          var momval = this.iter < 250 ? 0.5 : 0.8;
          var newsid = momval * sid - this.epsilon * newgain * grad[i][d];
          this.ystep[i][d] = newsid; // remember the step we took

          // step!
          this.Y[i][d] += newsid; 

          ymean[d] += this.Y[i][d]; // accumulate mean so that we can center later
        }
      }

      // reproject Y to be zero mean
      for(var i=0;i<N;i++) {
        for(var d=0;d<this.dim;d++) {
          this.Y[i][d] -= ymean[d]/N;
        }
      }

      //if(this.iter%100===0) console.log('iter ' + this.iter + ', cost: ' + cost);
      return cost; // return current cost
    },

    // for debugging: gradient check
    debugGrad: function() {
      var N = this.N;

      var cg = this.costGrad(this.Y); // evaluate gradient
      var cost = cg.cost;
      var grad = cg.grad;

      var e = 1e-5;
      for(var i=0;i<N;i++) {
        for(var d=0;d<this.dim;d++) {
          var yold = this.Y[i][d];

          this.Y[i][d] = yold + e;
          var cg0 = this.costGrad(this.Y);

          this.Y[i][d] = yold - e;
          var cg1 = this.costGrad(this.Y);
          
          var analytic = grad[i][d];
          var numerical = (cg0.cost - cg1.cost) / ( 2 * e );
          console.log(i + ',' + d + ': gradcheck analytic: ' + analytic + ' vs. numerical: ' + numerical);

          this.Y[i][d] = yold;
        }
      }
    },

    // return cost and gradient, given an arrangement
    costGrad: function(Y) {
      var N = this.N;
      var dim = this.dim; // dim of output space
      var P = this.P;

      var pmul = this.iter < 100 ? 4 : 1; // trick that helps with local optima

      // compute current Q distribution, unnormalized first
      var Qu = zeros(N * N);
      var qsum = 0.0;
      for(var i=0;i<N;i++) {
        for(var j=i+1;j<N;j++) {
          var dsum = 0.0;
          for(var d=0;d<dim;d++) {
            var dhere = Y[i][d] - Y[j][d];
            dsum += dhere * dhere;
          }
          var qu = 1.0 / (1.0 + dsum); // Student t-distribution
          Qu[i*N+j] = qu;
          Qu[j*N+i] = qu;
          qsum += 2 * qu;
        }
      }
      // normalize Q distribution to sum to 1
      var NN = N*N;
      var Q = zeros(NN);
      for(var q=0;q<NN;q++) { Q[q] = Math.max(Qu[q] / qsum, 1e-100); }

      var cost = 0.0;
      var grad = [];
      for(var i=0;i<N;i++) {
        var gsum = new Array(dim); // init grad for point i
        for(var d=0;d<dim;d++) { gsum[d] = 0.0; }
        for(var j=0;j<N;j++) {
          cost += - P[i*N+j] * Math.log(Q[i*N+j]); // accumulate cost (the non-constant portion at least...)
          var premult = 4 * (pmul * P[i*N+j] - Q[i*N+j]) * Qu[i*N+j];
          for(var d=0;d<dim;d++) {
            gsum[d] += premult * (Y[i][d] - Y[j][d]);
          }
        }
        grad.push(gsum);
      }

      return {cost: cost, grad: grad};
    }
  }

  global.tSNE = tSNE; // export tSNE class
})(tsnejs);


// export the library to window, or to module in nodejs
(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.tsnejs = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(tsnejs);

},{}]},{},[1]);
