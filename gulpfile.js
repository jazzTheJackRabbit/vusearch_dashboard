// requirements
var gulp = require('gulp');
var gulpBrowser = require("gulp-browser");
var reactify = require('reactify');
var del = require('del');
var size = require('gulp-size');


// tasks

gulp.task('transform', function () {
  var stream = gulp.src('./src/views/components/*.js')
    .pipe(gulpBrowser.browserify({transform: ['reactify']}))
    .pipe(gulp.dest('./src/static/compjs/'))
    .pipe(size());
  return stream;
});

gulp.task('del', function () {
  return del(['./src/static/compjs']);
});

gulp.task('default', ['del'], function () {
  gulp.start('transform');
  gulp.watch('./src/views/components/*.js', ['transform']);
});